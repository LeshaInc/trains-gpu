{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = with pkgs; [
    x11
    rustChannels.nightly.rust
    rustChannels.nightly.rustfmt-preview
  ];
}
