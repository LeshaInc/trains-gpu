use std::time::{Duration, Instant};

#[derive(Clone, Debug)]
pub struct FpsCounter {
    frames: Vec<Duration>,
    position: usize,
    frame_started: Instant,
}

impl FpsCounter {
    pub fn new(size: usize) -> FpsCounter {
        FpsCounter {
            frames: Vec::with_capacity(size),
            position: 0,
            frame_started: Instant::now(),
        }
    }

    pub fn begin_frame(&mut self) {
        self.frame_started = Instant::now();
    }

    pub fn end_frame(&mut self) {
        let elapsed = self.frame_started.elapsed();

        if self.position >= self.frames.len() {
            self.frames.push(elapsed);
        } else {
            self.frames[self.position] = elapsed;
        }

        self.position = (self.position + 1) % self.frames.capacity();
    }

    pub fn avg_frametime(&self) -> Duration {
        if self.frames.is_empty() {
            Duration::new(0, 0)
        } else {
            self.frames.iter().sum::<Duration>() / (self.frames.len() as u32)
        }
    }

    pub fn avg_fps(&self) -> f64 {
        // dbg!(self.frames.len());
        if self.frames.is_empty() {
            0.0
        } else {
            (self.frames.len() as f64) / self.frames.iter().sum::<Duration>().as_secs_f64()
        }
    }
}
