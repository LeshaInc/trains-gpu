use std::{
    any::{type_name, Any},
    marker::PhantomData,
};

pub struct Relevant<T: Any>(PhantomData<T>);

impl<T: Any> std::fmt::Debug for Relevant<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Relevant")
    }
}

impl<T: Any> Clone for Relevant<T> {
    fn clone(&self) -> Self {
        Relevant(PhantomData)
    }
}

impl<T: Any> Relevant<T> {
    pub fn new() -> Relevant<T> {
        Relevant(PhantomData)
    }

    pub fn dispose(self) {
        std::mem::forget(self);
    }
}

impl<T: Any> std::ops::Drop for Relevant<T> {
    fn drop(&mut self) {
        log::error!(
            "MEMORY LEAK: Values of type `{}` cannot be dropped",
            type_name::<T>()
        )
    }
}
