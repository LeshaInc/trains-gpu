use std::fmt::{self, Debug};

#[derive(Clone, Debug)]
pub struct FrameTracker {
    current_frame: Frame,
    frames_in_flight: u32,
}

impl FrameTracker {
    pub fn new(frames_in_flight: u32) -> FrameTracker {
        assert!(frames_in_flight > 0);
        FrameTracker {
            frames_in_flight,
            current_frame: Frame { index: 0, slot: 0 },
        }
    }

    pub fn frames_in_flight(&self) -> u32 {
        self.frames_in_flight
    }

    pub fn next(&mut self) {
        let cur = &mut self.current_frame;
        cur.index += 1;
        cur.slot += 1;
        if cur.slot == self.frames_in_flight {
            cur.slot = 0;
        }
    }

    pub fn current(&self) -> Frame {
        self.current_frame
    }
}

#[derive(Clone, Copy, Eq, Ord, PartialEq, PartialOrd)]
pub struct Frame {
    index: u32,
    slot: u32,
}

impl Frame {
    pub fn index(self) -> u32 {
        self.index
    }

    // usize here is useful for array indexing
    pub fn slot(self) -> usize {
        self.slot as _
    }
}

impl Debug for Frame {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}({})", self.index, self.slot)
    }
}

#[derive(Clone, Debug)]
pub struct FrameData<T> {
    data: Box<[T]>,
}

impl<T> FrameData<T> {
    pub fn new<E>(
        tracker: &FrameTracker,
        init: impl FnMut(usize) -> Result<T, E>,
    ) -> Result<FrameData<T>, E> {
        let data = (0..tracker.frames_in_flight as usize)
            .map(init)
            .collect::<Result<_, _>>()?;
        Ok(FrameData { data })
    }

    pub fn new_infallible(tracker: &FrameTracker, init: impl FnMut(usize) -> T) -> FrameData<T> {
        let data = (0..tracker.frames_in_flight as usize).map(init).collect();
        FrameData { data }
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> + '_ {
        self.data.iter()
    }

    pub fn drain(self) -> impl Iterator<Item = T> {
        self.data.into_vec().into_iter()
    }
}

impl<T> std::ops::Index<Frame> for FrameData<T> {
    type Output = T;

    fn index(&self, index: Frame) -> &T {
        &self.data[index.slot()]
    }
}

impl<T> std::ops::IndexMut<Frame> for FrameData<T> {
    fn index_mut(&mut self, index: Frame) -> &mut T {
        &mut self.data[index.slot()]
    }
}
