use rusttype::Font;
use vek::{Aabr, Rgba, Vec2};

#[derive(Clone, Copy, Debug)]
pub struct BorderRadii {
    pub bottom_right: f32,
    pub top_right: f32,
    pub bottom_left: f32,
    pub top_left: f32,
}

impl BorderRadii {
    pub fn new(v: f32) -> BorderRadii {
        BorderRadii {
            bottom_right: v,
            top_right: v,
            bottom_left: v,
            top_left: v,
        }
    }
}

impl Into<[f32; 4]> for BorderRadii {
    fn into(self) -> [f32; 4] {
        [
            self.bottom_right,
            self.top_right,
            self.bottom_left,
            self.top_left,
        ]
    }
}

pub struct Rect {
    pub origin: Vec2<f32>,
    pub size: Vec2<f32>,
    pub color: Rgba<f32>,
    pub shadow_color: Rgba<f32>,
    pub shadow_offset: Vec2<f32>,
    pub shadow_blur_radius: f32,
    pub border_radii: BorderRadii,
    pub border_color: Rgba<f32>,
    pub border_thickness: f32,
}

pub struct Text {
    pub origin: Vec2<f32>,
    pub font_size: Vec2<f32>,
    pub color: Rgba<f32>,
    pub text: String,
    pub font_id: usize,
}

pub enum DrawCommand {
    Rect(Rect),
    Text(Text),
}

pub struct BlurCommand {
    pub region: Aabr<u32>,
    pub radius: f32,
}

pub enum Command {
    Draw(DrawCommand),
    Blur(BlurCommand),
}

pub struct DrawList {
    pub commands: Vec<Command>,
    pub fonts: Vec<Font<'static>>,
}
