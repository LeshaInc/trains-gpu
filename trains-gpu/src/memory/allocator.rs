use std::ptr::NonNull;

use gfx_hal::{adapter::MemoryProperties, Backend, MemoryTypeId};

use anyhow::{anyhow, Result};

use super::{
    pool::{DedicatedPool, DynPool, Pool, PoolMeta},
    Memory, MemoryMapping, MemorySpec,
};
use crate::util::Pod;

#[derive(Debug)]
pub struct HeapMeta {
    pub total: u64,
    pub used: u64,
}

#[derive(Debug)]
pub struct Allocator<B: Backend> {
    pools: Vec<DynPool<B>>,
    heaps: Vec<HeapMeta>,
}

impl<B: Backend> Allocator<B> {
    pub fn new(props: &MemoryProperties) -> Allocator<B> {
        let mut pools = Vec::with_capacity(props.memory_types.len());

        for (id, ty) in props.memory_types.iter().enumerate() {
            let type_id = MemoryTypeId(id);
            let properties = ty.properties;
            let mut meta = PoolMeta {
                pool_id: pools.len() as _,
                heap_id: ty.heap_index as _,
                type_id,
                properties,
            };

            pools.push(DedicatedPool::new(meta, Default::default()).into());
            meta.pool_id += 1;
        }

        let heaps = props
            .memory_heaps
            .iter()
            .map(|&total| HeapMeta { total, used: 0 })
            .collect();

        Allocator { pools, heaps }
    }

    pub fn allocate(&mut self, device: &B::Device, mut spec: MemorySpec) -> Result<Memory<B>> {
        spec.usage_to_properties();

        let (pool_id, _) = self
            .pools
            .iter()
            .enumerate()
            .filter(|(_, pool)| {
                let meta = pool.meta();
                let heap = &self.heaps[meta.heap_id as usize];
                meta.satisfies_spec(&spec) && pool.can_allocate(heap, &spec)
            })
            .max_by_key(|(_, p)| p.score(&spec) + p.meta().type_score(&spec))
            .ok_or_else(|| anyhow!("Can't find suitable memory"))?;

        let pool = &mut self.pools[pool_id];
        let heap = &mut self.heaps[pool.meta().heap_id as usize];
        pool.allocate(device, heap, spec)
    }

    /// Safety: memory must not be in use by device
    pub unsafe fn free(&mut self, device: &B::Device, memory: Memory<B>) {
        let pool = &mut self.pools[memory.pool as usize];
        let heap = &mut self.heaps[pool.meta().heap_id as usize];
        pool.free(device, heap, memory);
    }

    /// Safety: memory must not be in use by device
    pub unsafe fn map<'a, T: Pod>(
        &mut self,
        device: &B::Device,
        memory: &'a mut Memory<B>,
    ) -> Result<MemoryMapping<'a, B, T>> {
        let pool = &mut self.pools[memory.pool as usize];
        let ptr = pool.map(device, memory)?;
        let ptr = NonNull::new(ptr as *mut T)
            .ok_or_else(|| anyhow!("Pool::map_memory returned a null pointer"))?;
        Ok(MemoryMapping { memory, ptr })
    }

    /// Safety: memory must not be in use by device
    pub unsafe fn unmap<T: Pod>(&mut self, device: &B::Device, mapping: MemoryMapping<'_, B, T>) {
        let pool = &mut self.pools[mapping.memory.pool as usize];
        pool.unmap(device);
    }
}
