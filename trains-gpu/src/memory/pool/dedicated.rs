use gfx_hal::{
    device::Device,
    memory::{Properties, Segment},
    Backend,
};

use anyhow::{anyhow, Context, Result};

use crate::{
    memory::{HeapMeta, Memory, MemorySpec},
    util::Relevant,
};

use super::{Pool, PoolMeta};

#[derive(Debug)]
pub struct DedicatedConfig {
    pub size_treshold: u64,
}

impl Default for DedicatedConfig {
    fn default() -> DedicatedConfig {
        DedicatedConfig {
            size_treshold: 256 * 1024 * 1024,
        }
    }
}

#[repr(C)]
struct MemoryEntry<B: Backend> {
    memory: B::Memory,
    mapping: Option<*mut u8>,
}

#[derive(Debug)]
pub struct DedicatedPool<B: Backend> {
    meta: PoolMeta,
    config: DedicatedConfig,
    _marker: std::marker::PhantomData<B>,
}

impl<B: Backend> DedicatedPool<B> {
    pub fn new(meta: PoolMeta, config: DedicatedConfig) -> DedicatedPool<B> {
        DedicatedPool {
            meta,
            config,
            _marker: std::marker::PhantomData,
        }
    }
}

impl<B: Backend> Pool<B> for DedicatedPool<B> {
    fn meta(&self) -> &PoolMeta {
        &self.meta
    }

    fn score(&self, spec: &MemorySpec) -> i32 {
        if spec.prefers_dedicated {
            100
        } else if spec.requirements.size < self.config.size_treshold {
            -100
        } else {
            0
        }
    }

    fn can_allocate(&self, heap: &HeapMeta, spec: &MemorySpec) -> bool {
        heap.used + spec.requirements.size + 1024 <= heap.total
    }

    fn allocate(
        &mut self,
        device: &B::Device,
        heap: &mut HeapMeta,
        spec: MemorySpec,
    ) -> Result<Memory<B>> {
        debug_assert!(self.meta.satisfies_spec(&spec));

        let memory = unsafe {
            device
                .allocate_memory(self.meta.type_id, spec.requirements.size)
                .context("Can't allocate memory")?
        };

        heap.used += spec.requirements.size;

        let mapping = if self.meta.properties.contains(Properties::CPU_VISIBLE) {
            Some(unsafe {
                device
                    .map_memory(&memory, Segment::ALL)
                    .context("Can't map memory")?
            })
        } else {
            None
        };

        let entry = MemoryEntry::<B> { mapping, memory };
        let ptr = Box::into_raw(Box::new(entry)) as *const _;

        Ok(Memory {
            ptr,
            range: 0..spec.requirements.size,
            pool: self.meta.pool_id,
            properties: self.meta.properties,
            aux: 0,
            relevant: Relevant::new(),
        })
    }

    unsafe fn free(&mut self, device: &B::Device, heap: &mut HeapMeta, memory: Memory<B>) {
        debug_assert_eq!(memory.pool, self.meta.pool_id);
        heap.used -= memory.range().end - memory.range().start;
        memory.relevant.dispose();

        let entry = Box::from_raw(memory.ptr as *mut MemoryEntry<B>);
        if entry.mapping.is_some() {
            device.unmap_memory(&entry.memory);
        }

        device.free_memory(entry.memory);
    }

    unsafe fn map(&mut self, _device: &B::Device, memory: &mut Memory<B>) -> Result<*mut u8> {
        debug_assert_eq!(memory.pool, self.meta.pool_id);
        let entry = &*(memory.ptr as *mut MemoryEntry<B>);
        entry
            .mapping
            .ok_or_else(|| anyhow!("Memory is not mappable"))
    }

    unsafe fn unmap(&mut self, _device: &B::Device) {}
}
