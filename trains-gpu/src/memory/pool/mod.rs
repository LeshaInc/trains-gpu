mod dedicated;

pub use self::dedicated::DedicatedPool;

use gfx_hal::{memory::Properties, Backend, MemoryTypeId};

use anyhow::Result;

use crate::memory::{HeapMeta, Memory, MemorySpec};

#[derive(Clone, Copy, Debug)]
pub struct PoolMeta {
    pub pool_id: u16,
    pub heap_id: u16,
    pub properties: Properties,
    pub type_id: MemoryTypeId,
}

impl PoolMeta {
    pub fn satisfies_spec(&self, spec: &MemorySpec) -> bool {
        self.properties.contains(spec.properties.required)
            && spec.requirements.type_mask & (1 << self.type_id.0) != 0
            && spec.requirements.size > 0
    }

    pub fn type_score(&self, spec: &MemorySpec) -> i32 {
        let preferred = (self.properties & spec.properties.preferred)
            .bits()
            .count_ones();
        let not_preferred = (self.properties & spec.properties.not_preferred)
            .bits()
            .count_ones();
        (preferred as i32) - (not_preferred as i32)
    }
}

pub trait Pool<B: Backend>: std::fmt::Debug + Send + Sync {
    fn meta(&self) -> &PoolMeta;

    fn score(&self, spec: &MemorySpec) -> i32 {
        let _ = spec;
        0
    }

    fn can_allocate(&self, heap: &HeapMeta, spec: &MemorySpec) -> bool {
        let _ = (heap, spec);
        true
    }

    fn allocate(
        &mut self,
        device: &B::Device,
        heap: &mut HeapMeta,
        spec: MemorySpec,
    ) -> Result<Memory<B>>;

    /// Safety: memory must be allocated from this pool, and not be in use by the device
    unsafe fn free(&mut self, device: &B::Device, heap: &mut HeapMeta, memory: Memory<B>);

    /// Safety: memory must be allocated from this pool, and not be in use by the device
    unsafe fn map(&mut self, device: &B::Device, memory: &mut Memory<B>) -> Result<*mut u8>;

    /// Safety: memory must be allocated from this pool, mapped, and not in use by the device
    unsafe fn unmap(&mut self, device: &B::Device);
}

#[derive(Debug)]
pub enum DynPool<B: Backend> {
    Dedicated(DedicatedPool<B>),
    Boxed(Box<dyn Pool<B>>),
}

impl<B: Backend> From<DedicatedPool<B>> for DynPool<B> {
    fn from(v: DedicatedPool<B>) -> DynPool<B> {
        DynPool::Dedicated(v)
    }
}

macro_rules! delegate {
    ($s:ident, $name:ident => $expr:expr) => {
        match $s {
            Self::Dedicated($name) => $expr,
            Self::Boxed($name) => $expr,
        }
    };
}

impl<B: Backend> Pool<B> for DynPool<B> {
    fn meta(&self) -> &PoolMeta {
        delegate!(self, v => v.meta())
    }

    fn score(&self, spec: &MemorySpec) -> i32 {
        delegate!(self, v => v.score(spec))
    }

    fn can_allocate(&self, heap: &HeapMeta, spec: &MemorySpec) -> bool {
        delegate!(self, v => v.can_allocate(heap, spec))
    }

    fn allocate(
        &mut self,
        device: &B::Device,
        heap: &mut HeapMeta,
        spec: MemorySpec,
    ) -> Result<Memory<B>> {
        delegate!(self, v => v.allocate(device, heap, spec))
    }

    unsafe fn free(&mut self, device: &B::Device, heap: &mut HeapMeta, memory: Memory<B>) {
        delegate!(self, v => v.free(device, heap, memory))
    }

    unsafe fn map(&mut self, device: &B::Device, memory: &mut Memory<B>) -> Result<*mut u8> {
        delegate!(self, v => v.map(device, memory))
    }

    unsafe fn unmap(&mut self, device: &B::Device) {
        delegate!(self, v => v.unmap(device))
    }
}
