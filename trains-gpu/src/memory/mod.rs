pub mod pool;

mod allocator;
mod spec;

pub use self::{
    allocator::{Allocator, HeapMeta},
    spec::{MemorySpec, MemoryUsage, PropertiesSpec},
};

use std::{mem::MaybeUninit, ops::Range, ptr::NonNull};

use gfx_hal::{
    device::Device,
    memory::{Properties, Segment},
    Backend,
};

use anyhow::{Context, Result};

use crate::util::{Pod, Relevant};

#[derive(Debug)]
pub struct Memory<B: Backend> {
    ptr: *const B::Memory,
    range: Range<u64>,
    pool: u16,
    properties: Properties,
    aux: u32,
    relevant: Relevant<Self>,
}

unsafe impl<B: Backend> Send for Memory<B> {}
unsafe impl<B: Backend> Sync for Memory<B> {}

impl<B: Backend> Memory<B> {
    pub fn inner(&self) -> &B::Memory {
        unsafe { &*self.ptr }
    }

    pub fn range(&self) -> Range<u64> {
        self.range.start..self.range.end
    }

    pub fn properties(&self) -> Properties {
        self.properties
    }

    pub fn is_visible(&self) -> bool {
        self.properties.contains(Properties::CPU_VISIBLE)
    }
}

pub struct MemoryMapping<'a, B: Backend, T: Pod> {
    memory: &'a mut Memory<B>,
    ptr: NonNull<T>,
}

impl<B: Backend, T: Pod> MemoryMapping<'_, B, T> {
    pub fn is_coherent(&self) -> bool {
        self.memory.properties.contains(Properties::COHERENT)
    }

    fn map_range(&self, range: Range<usize>) -> Segment {
        let stride = std::mem::size_of::<T>() as u64;
        let start = self.memory.range().start + stride * (range.start as u64);
        let len = stride * (range.end - range.start) as u64;
        Segment {
            offset: start,
            size: Some(len),
        }
    }

    pub fn invalidate(&mut self, device: &B::Device, range: Range<usize>) -> Result<()> {
        if self.is_coherent() {
            return Ok(());
        }

        let segment = self.map_range(range);
        unsafe {
            device
                .invalidate_mapped_memory_ranges(Some((self.memory.inner(), segment)))
                .context("Can't invalidate mapped memory range")
        }
    }

    pub fn flush(&mut self, device: &B::Device, range: Range<usize>) -> Result<()> {
        if self.is_coherent() {
            return Ok(());
        }

        let segment = self.map_range(range);
        unsafe {
            device
                .flush_mapped_memory_ranges(Some((self.memory.inner(), segment)))
                .context("Can't flush mapped memory range")
        }
    }

    pub fn copy_from_slice(&mut self, range: Range<usize>, source: &[T]) {
        let source_maybe_uninit = unsafe { std::mem::transmute(source) };
        self.as_uninit_slice()[range.start..range.end].copy_from_slice(source_maybe_uninit)
    }

    pub fn as_uninit_slice(&mut self) -> &mut [MaybeUninit<T>] {
        let stride = std::mem::size_of::<T>() as u64;
        let range = self.memory.range();
        let len = ((range.end - range.start) / stride) as usize;
        unsafe { std::slice::from_raw_parts_mut(self.ptr.as_ptr() as _, len) }
    }
}
