use gfx_hal::memory::{Properties, Requirements};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum MemoryUsage {
    Unknown,
    GpuOnly,
    CpuOnly,
    CpuToGpu,
    GpuToCpu,
    CpuCopy,
}

impl Default for MemoryUsage {
    fn default() -> MemoryUsage {
        MemoryUsage::Unknown
    }
}

impl MemoryUsage {
    pub fn to_properties(self) -> PropertiesSpec {
        let mut spec = PropertiesSpec::default();

        match self {
            MemoryUsage::GpuOnly => spec.preferred |= Properties::DEVICE_LOCAL,
            MemoryUsage::CpuOnly => spec.required |= Properties::CPU_VISIBLE | Properties::COHERENT,
            MemoryUsage::CpuToGpu => {
                spec.required |= Properties::CPU_VISIBLE;
                spec.preferred |= Properties::DEVICE_LOCAL;
            }
            MemoryUsage::GpuToCpu => {
                spec.required |= Properties::CPU_VISIBLE;
                spec.preferred |= Properties::CPU_CACHED;
            }
            MemoryUsage::CpuCopy => {
                spec.required |= Properties::CPU_VISIBLE;
                spec.not_preferred |= Properties::DEVICE_LOCAL;
            }
            _ => (),
        }

        spec
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct PropertiesSpec {
    pub required: Properties,
    pub preferred: Properties,
    pub not_preferred: Properties,
}

impl Default for PropertiesSpec {
    fn default() -> PropertiesSpec {
        PropertiesSpec {
            required: Properties::empty(),
            preferred: Properties::empty(),
            not_preferred: Properties::empty(),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct MemorySpec {
    pub requirements: Requirements,
    pub usage: MemoryUsage,
    pub properties: PropertiesSpec,
    pub prefers_dedicated: bool,
}

impl MemorySpec {
    pub fn new(requirements: Requirements) -> MemorySpec {
        MemorySpec {
            requirements,
            usage: MemoryUsage::Unknown,
            properties: PropertiesSpec::default(),
            prefers_dedicated: false,
        }
    }

    pub fn with_usage(mut self, usage: MemoryUsage) -> MemorySpec {
        self.usage = usage;
        self
    }

    pub fn with_required_properties(mut self, properties: Properties) -> MemorySpec {
        self.properties.required |= properties;
        self
    }

    pub fn with_preferred_properties(mut self, properties: Properties) -> MemorySpec {
        self.properties.preferred |= properties;
        self
    }

    pub fn with_not_preferred_properties(mut self, properties: Properties) -> MemorySpec {
        self.properties.not_preferred |= properties;
        self
    }

    pub fn usage_to_properties(&mut self) {
        let usage_properties = self.usage.to_properties();
        self.properties.required |= usage_properties.required;
        self.properties.preferred |= usage_properties.preferred;
        self.properties.not_preferred |= usage_properties.not_preferred;
    }
}
