use std::iter::once;

use gfx_hal::{
    command::{CommandBuffer, SubpassContents},
    device::Device,
    format::Format,
    image::{Filter, SamplerDesc, WrapMode},
    pass::{Attachment, AttachmentOps, SubpassDesc},
    pso::{
        Descriptor, DescriptorPool, DescriptorRangeDesc, DescriptorSetLayoutBinding,
        DescriptorSetWrite, DescriptorType, ImageDescriptorType, ShaderStageFlags,
    },
    Backend,
};

use anyhow::{Context, Result};

use crate::{
    gpu::Gpu,
    image::ImageLayout,
    renderer::{
        context::ResizeContext,
        shader::{ShaderKind, ShaderModule},
        util::basic_pipeline_desc,
        FrameContext,
    },
};

pub struct CopyPipeline<B: Backend> {
    render_pass: B::RenderPass,
    pipeline: B::GraphicsPipeline,
    pipeline_layout: B::PipelineLayout,
    desc_pool: B::DescriptorPool,
    desc_set: B::DescriptorSet,
    desc_set_layout: B::DescriptorSetLayout,
    sampler: B::Sampler,
}

impl<B: Backend> CopyPipeline<B> {
    pub fn new(gpu: &Gpu<B>) -> Result<Self> {
        let attachment = Attachment {
            format: Some(Format::Bgra8Srgb),
            samples: 1,
            ops: AttachmentOps::PRESERVE,
            stencil_ops: AttachmentOps::DONT_CARE,
            layouts: ImageLayout::Undefined..ImageLayout::Present,
        };

        let subpass = SubpassDesc {
            colors: &[(0, ImageLayout::ColorAttachmentOptimal)],
            depth_stencil: None,
            inputs: &[],
            resolves: &[],
            preserves: &[],
        };

        let render_pass = unsafe {
            gpu.device()
                .create_render_pass(&[attachment], &[subpass], &[])
                .context("Can't create render pass")?
        };

        let device = gpu.device();
        let desc_set_layout = unsafe {
            device.create_descriptor_set_layout(
                &[DescriptorSetLayoutBinding {
                    binding: 0,
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 1,
                    stage_flags: ShaderStageFlags::FRAGMENT,
                    immutable_samplers: false,
                }],
                &[],
            )
        }
        .context("Can't create descriptor set layout")?;

        let mut desc_pool = unsafe {
            device.create_descriptor_pool(
                1,
                &[DescriptorRangeDesc {
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 1,
                }],
                gfx_hal::pso::DescriptorPoolCreateFlags::empty(),
            )
        }
        .context("Can't create descriptor pool")?;

        let desc_set = unsafe { desc_pool.allocate_set(&desc_set_layout) }
            .context("Can't allocate descriptor set")?;

        let sampler =
            unsafe { device.create_sampler(&SamplerDesc::new(Filter::Linear, WrapMode::Clamp)) }
                .context("Can't create sampler")?;

        let pipeline_layout = unsafe {
            gpu.device()
                .create_pipeline_layout(once(&desc_set_layout), &[])
                .context("Can't create pipeline layout")?
        };

        let vert_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/copy.vert", ShaderKind::Vertex)
                .context("Can't compile vertex shader")?;
        let frag_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/copy.frag", ShaderKind::Fragment)
                .context("Can't compile fragment shader")?;

        let desc = basic_pipeline_desc(&vert_shader, &frag_shader, &pipeline_layout, &render_pass);
        let pipeline = unsafe {
            gpu.device()
                .create_graphics_pipeline(&desc, None)
                .context("Can't create graphics pipeline")?
        };

        unsafe {
            frag_shader.dispose(gpu);
            vert_shader.dispose(gpu);
        }

        Ok(CopyPipeline {
            render_pass,
            pipeline,
            pipeline_layout,
            desc_pool,
            desc_set,
            desc_set_layout,
            sampler,
        })
    }

    pub fn resize(&mut self, ctx: &mut ResizeContext<'_, B>) {
        unsafe {
            ctx.gpu
                .device()
                .write_descriptor_sets(vec![DescriptorSetWrite {
                    set: &self.desc_set,
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::CombinedImageSampler(
                        ctx.target.view_raw(),
                        ImageLayout::ShaderReadOnlyOptimal,
                        &self.sampler,
                    )),
                }]);
        }
    }

    pub fn encode(&mut self, ctx: &mut FrameContext<'_, B>) -> Result<()> {
        let framebuffer = unsafe {
            ctx.gpu.device().create_framebuffer(
                &self.render_pass,
                once(ctx.window_target),
                ctx.image_extent(),
            )?
        };

        unsafe {
            ctx.cbuf.begin_render_pass(
                &self.render_pass,
                &framebuffer,
                ctx.viewport().rect,
                &[],
                SubpassContents::Inline,
            );
            ctx.cbuf.bind_graphics_descriptor_sets(
                &self.pipeline_layout,
                0,
                once(&self.desc_set),
                &[],
            );
            ctx.cbuf.bind_graphics_pipeline(&self.pipeline);
            ctx.cbuf.draw(0..6, 0..1);
            ctx.cbuf.end_render_pass();
        }

        ctx.cleaner.dispose_framebuffer(framebuffer);

        Ok(())
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        gpu.device().destroy_pipeline_layout(self.pipeline_layout);
        gpu.device().destroy_graphics_pipeline(self.pipeline);
        gpu.device().destroy_descriptor_pool(self.desc_pool);
        gpu.device()
            .destroy_descriptor_set_layout(self.desc_set_layout);
        gpu.device().destroy_sampler(self.sampler);
        gpu.device().destroy_render_pass(self.render_pass)
    }
}
