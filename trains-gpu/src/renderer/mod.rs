mod context;
mod shader;
mod util;

mod canvas;
mod copy;

use std::{borrow::Borrow, mem::ManuallyDrop, sync::Arc};

use anyhow::{Context, Result};
use vek::Extent2;

use gfx_hal::{
    command::{CommandBuffer, CommandBufferFlags},
    device::Device,
    format::{Aspects, Format},
    image::SubresourceRange,
    memory::{Barrier, Dependencies},
    pso::PipelineStage,
    queue::{CommandQueue, Submission},
    window::{AcquireError, Extent2D, PresentError, PresentationSurface, Surface, SwapchainConfig},
    Backend, Instance as _,
};

use crate::{
    canvas::DrawList,
    cleaner::FrameCleaner,
    command::{CommandCirque, TransferManager},
    gpu::{Gpu, QueueType},
    image::{Image, ImageAccess, ImageDesc, ImageLayout, ImageUsage},
    util::{FrameData, FrameTracker},
};

use self::{
    canvas::CanvasRenderer,
    context::{FrameContext, ResizeContext},
    copy::CopyPipeline,
};

use winit::window::Window;

type SwImage<B> = <<B as Backend>::Surface as PresentationSurface<B>>::SwapchainImage;

pub struct Renderer<B: Backend> {
    gpu: Gpu<B>,
    transfer_manager: ManuallyDrop<TransferManager<B>>,
    surface: ManuallyDrop<B::Surface>,
    frame_tracker: FrameTracker,
    command_cirque: ManuallyDrop<CommandCirque<B>>,
    frame_semaphores: ManuallyDrop<FrameData<B::Semaphore>>,
    resolution: Extent2<u16>,
    window: Arc<Window>,
    image: Image<B>,
    cleaner: FrameCleaner<B>,
    canvas_renderer: ManuallyDrop<CanvasRenderer<B>>,
    copy_pipeline: ManuallyDrop<CopyPipeline<B>>,
}

impl<B: Backend> Renderer<B> {
    pub fn new(instance: B::Instance, window: Arc<Window>) -> Result<Renderer<B>> {
        let surface = unsafe { instance.create_surface(window.as_ref())? };

        let gpu = Gpu::new(instance, &surface)?;
        let mut transfer_manager = TransferManager::new(&gpu)?;

        let frame_tracker = FrameTracker::new(3);

        let resolution = Extent2::new(800, 600);
        let desc = ImageDesc::new_2d(800, 600)
            .with_format(Format::Bgra8Srgb)
            .with_usage(ImageUsage::COLOR_ATTACHMENT | ImageUsage::SAMPLED);

        let image = gpu.create_image(&desc)?;

        let canvas_renderer = {
            let mut transfer_encoder = transfer_manager.new_encoder();
            let renderer = CanvasRenderer::new(&gpu, &mut transfer_encoder, &image, resolution)?;
            transfer_manager.submit(transfer_encoder);
            ManuallyDrop::new(renderer)
        };

        let copy_pipeline = CopyPipeline::new(&gpu)?;

        transfer_manager.begin_frame(&gpu)?;
        transfer_manager.flush(&gpu);
        transfer_manager.wait_for_all(&gpu)?;

        let command_cirque = CommandCirque::new(&gpu, &frame_tracker, QueueType::Graphics)?;

        let frame_semaphores = FrameData::new(&frame_tracker, |_| {
            let device: &B::Device = gpu.device();
            device.create_semaphore()
        })?;

        Ok(Renderer {
            gpu,
            cleaner: FrameCleaner::new(&frame_tracker),
            transfer_manager: ManuallyDrop::new(transfer_manager),
            surface: ManuallyDrop::new(surface),
            frame_tracker,
            command_cirque: ManuallyDrop::new(command_cirque),
            frame_semaphores: ManuallyDrop::new(frame_semaphores),
            resolution,
            window,
            canvas_renderer,
            copy_pipeline: ManuallyDrop::new(copy_pipeline),
            image,
        })
    }

    pub fn resize(&mut self) -> Result<()> {
        self.recreate_swapchain()?;

        let desc = ImageDesc::new_2d(self.resolution.w as _, self.resolution.h as _)
            .with_format(Format::Bgra8Srgb)
            .with_usage(ImageUsage::COLOR_ATTACHMENT | ImageUsage::SAMPLED);

        let image = self.gpu.create_image(&desc)?;
        let old_image = std::mem::replace(&mut self.image, image);
        self.cleaner.dispose_image(old_image);

        let mut ctx = ResizeContext {
            gpu: &self.gpu,
            cleaner: &mut self.cleaner,
            target: &self.image,
            resolution: self.resolution,
        };
        self.canvas_renderer.resize(&mut ctx)?;
        self.copy_pipeline.resize(&mut ctx);

        Ok(())
    }

    fn recreate_swapchain(&mut self) -> Result<()> {
        let res = self.window.inner_size();

        let caps = self.surface.capabilities(self.gpu.physical_device());
        let config = SwapchainConfig::from_caps(
            &caps,
            Format::Bgra8Srgb,
            Extent2D {
                width: res.width,
                height: res.height,
            },
        )
        .with_present_mode(gfx_hal::window::PresentMode::IMMEDIATE);

        self.resolution[0] = config.extent.width as _;
        self.resolution[1] = config.extent.height as _;

        unsafe {
            self.surface
                .configure_swapchain(self.gpu.device(), config)
                .context("Can't configure swapchain")
        }
    }

    pub fn init(&mut self) -> Result<()> {
        self.resize()
    }

    pub fn render(&mut self, list: &DrawList) -> Result<()> {
        self.command_cirque
            .begin_frame(&self.gpu, self.frame_tracker.current())?;

        unsafe {
            // By this time we are sure frame in current slot has finished execution
            self.cleaner.cleanup(&self.gpu);
        }

        let target = self.acquire_image()?;

        let mut cbuf = self.command_cirque.get_primary_buffer();

        unsafe {
            cbuf.begin(CommandBufferFlags::ONE_TIME_SUBMIT, Default::default());
        }

        let mut ctx = FrameContext {
            gpu: &self.gpu,
            cbuf: &mut cbuf,
            target: &self.image,
            window_target: target.borrow(),
            resolution: Extent2::new(self.resolution[0] as _, self.resolution[1] as _),
            cleaner: &mut self.cleaner,
            frame: self.frame_tracker.current(),
        };

        let range = SubresourceRange {
            aspects: Aspects::COLOR,
            levels: 0..1,
            layers: 0..1,
        };

        unsafe {
            ctx.cbuf.pipeline_barrier(
                PipelineStage::FRAGMENT_SHADER..PipelineStage::COLOR_ATTACHMENT_OUTPUT,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (ImageAccess::empty(), ImageLayout::Undefined)
                        ..(
                            ImageAccess::COLOR_ATTACHMENT_WRITE,
                            ImageLayout::ColorAttachmentOptimal,
                        ),
                    target: ctx.target.raw(),
                    range: range.clone(),
                    families: None,
                }],
            );
        }

        self.canvas_renderer.encode(&mut ctx, list)?;

        unsafe {
            ctx.cbuf.pipeline_barrier(
                PipelineStage::COLOR_ATTACHMENT_OUTPUT..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (
                        ImageAccess::COLOR_ATTACHMENT_WRITE,
                        ImageLayout::ColorAttachmentOptimal,
                    )
                        ..(ImageAccess::SHADER_READ, ImageLayout::ShaderReadOnlyOptimal),
                    target: ctx.target.raw(),
                    range,
                    families: None,
                }],
            );
        }

        self.copy_pipeline.encode(&mut ctx)?;

        let mut should_resize = false;

        unsafe {
            let mut queue = self.gpu.queues().lock_graphics();
            let semaphore = &self.frame_semaphores[self.frame_tracker.current()];

            cbuf.finish();

            queue.submit(
                Submission {
                    command_buffers: &[&cbuf],
                    wait_semaphores: None,
                    signal_semaphores: Some(semaphore),
                },
                Some(self.command_cirque.current_fence()),
            );

            let res = queue.present_surface(&mut self.surface, target, Some(semaphore));
            match res {
                Ok(_) => (),
                Err(PresentError::OutOfDate) => {
                    should_resize = true;
                }
                Err(e) => return Err(e.into()),
            }
        }

        self.command_cirque.return_primary_buffer(cbuf);
        self.frame_tracker.next();
        self.cleaner.next_frame();

        if should_resize {
            self.resize()?;
        }

        Ok(())
    }

    fn acquire_image(&mut self) -> Result<SwImage<B>> {
        let res = unsafe { self.surface.acquire_image(!0) };
        match res {
            Ok((image, _)) => Ok(image),
            Err(AcquireError::OutOfDate) => {
                self.recreate_swapchain()?;
                self.acquire_image()
            }
            Err(e) => Err(e.into()),
        }
    }
}

impl<B: Backend> std::ops::Drop for Renderer<B> {
    fn drop(&mut self) {
        unsafe {
            let _ = self.gpu.device().wait_idle();

            for semaphore in ManuallyDrop::take(&mut self.frame_semaphores).drain() {
                self.gpu.device().destroy_semaphore(semaphore)
            }

            ManuallyDrop::take(&mut self.command_cirque).dispose(&self.gpu);
            ManuallyDrop::take(&mut self.canvas_renderer).dispose(&self.gpu);
            ManuallyDrop::take(&mut self.copy_pipeline).dispose(&self.gpu);
            ManuallyDrop::take(&mut self.transfer_manager).dispose(&self.gpu);

            self.surface.unconfigure_swapchain(self.gpu.device());

            self.gpu
                .instance()
                .destroy_surface(ManuallyDrop::take(&mut self.surface));
        }
    }
}
