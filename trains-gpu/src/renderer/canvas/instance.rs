use gfx_hal::{
    format::Format,
    pso::{AttributeDesc, Element, VertexBufferDesc, VertexInputRate},
};

use vek::{Aabr, Mat3};

use crate::{buffer::Vertex, canvas::Rect, util::Pod};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct UiInstance {
    transform_col0_1: [f32; 4],
    transform_col2_shadow_offset: [f32; 4],
    color: [f32; 4],
    shadow_color: [f32; 4],
    border_color: [f32; 4],
    border_radii: [f32; 4],
    size_blur_radius_thickness: [f32; 4],
}

impl UiInstance {
    pub fn new_rect(proj: Mat3<f32>, rect: &Rect) -> UiInstance {
        let aabr = Aabr {
            min: rect.origin,
            max: rect.origin + rect.size,
        };

        let shadow_aabr = Aabr {
            min: rect.origin + rect.shadow_offset - rect.shadow_blur_radius,
            max: rect.origin + rect.size + rect.shadow_offset + rect.shadow_blur_radius,
        };

        let combined = aabr.union(shadow_aabr);

        let model = Mat3::scaling_3d((combined.size().w, combined.size().h, 1.0))
            .translated_2d(combined.min);

        let transform = proj * model;

        let tr_col0 = transform.cols.x;
        let tr_col1 = transform.cols.y;
        let tr_col2 = transform.cols.z;

        UiInstance {
            transform_col0_1: [tr_col0.x, tr_col0.y, tr_col1.x, tr_col1.y],
            transform_col2_shadow_offset: [
                tr_col2.x,
                tr_col2.y,
                rect.shadow_offset.x,
                rect.shadow_offset.y,
            ],
            color: rect.color.into_array(),
            shadow_color: rect.shadow_color.into_array(),
            border_color: rect.border_color.into_array(),
            border_radii: rect.border_radii.into(),
            size_blur_radius_thickness: [
                rect.size.x,
                rect.size.y,
                rect.shadow_blur_radius,
                rect.border_thickness,
            ],
        }
    }
}

unsafe impl Pod for UiInstance {}

impl Vertex for UiInstance {
    fn vbuf_desc() -> VertexBufferDesc {
        VertexBufferDesc {
            binding: 1,
            stride: 112,
            rate: VertexInputRate::Instance(1),
        }
    }

    fn attributes() -> Vec<AttributeDesc> {
        (0..7)
            .map(|i| AttributeDesc {
                location: i,
                binding: 0,
                element: Element {
                    format: Format::Rgba32Sfloat,
                    offset: i * 16,
                },
            })
            .collect()
    }
}
