use gfx_hal::{
    format::Format,
    pso::{AttributeDesc, Element, VertexBufferDesc, VertexInputRate},
};

use crate::{buffer::Vertex, util::Pod};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct UiVertex {
    pub pos: [f32; 2],
}

impl UiVertex {
    pub const RECT: &'static [UiVertex] = &[
        UiVertex { pos: [0.0, 0.0] },
        UiVertex { pos: [1.0, 0.0] },
        UiVertex { pos: [0.0, 1.0] },
        UiVertex { pos: [0.0, 1.0] },
        UiVertex { pos: [1.0, 0.0] },
        UiVertex { pos: [1.0, 1.0] },
    ];
}

unsafe impl Pod for UiVertex {}

impl Vertex for UiVertex {
    fn vbuf_desc() -> VertexBufferDesc {
        VertexBufferDesc {
            binding: 0,
            stride: 8,
            rate: VertexInputRate::Vertex,
        }
    }

    fn attributes() -> Vec<AttributeDesc> {
        vec![AttributeDesc {
            location: 0,
            binding: 0,
            element: Element {
                format: Format::Rg32Sfloat,
                offset: 0,
            },
        }]
    }
}
