use std::iter::once;

use gfx_hal::{
    command::{CommandBuffer, SubpassContents},
    device::Device,
    format::{Aspects, Format},
    image::{Extent, Filter, SamplerDesc, SubresourceRange, WrapMode},
    memory::{Barrier, Dependencies},
    pass::{Attachment, AttachmentLoadOp, AttachmentOps, AttachmentStoreOp, SubpassDesc},
    pso::{
        Descriptor, DescriptorPool, DescriptorRangeDesc, DescriptorSetLayoutBinding,
        DescriptorSetWrite, DescriptorType, ImageDescriptorType, PipelineStage, ShaderStageFlags,
    },
    Backend,
};

use anyhow::{Context, Result};
use vek::{Aabr, Extent2};

use crate::{
    gpu::Gpu,
    image::{Image, ImageAccess, ImageDesc, ImageLayout, ImageUsage},
    renderer::{
        shader::{ShaderKind, ShaderModule},
        util::basic_pipeline_desc,
        FrameContext, ResizeContext,
    },
};

fn create_image_and_framebuffer<B: Backend>(
    gpu: &Gpu<B>,
    rpass: &B::RenderPass,
    resolution: Extent2<u16>,
) -> Result<(Image<B>, B::Framebuffer)> {
    let desc = ImageDesc::new_2d(resolution.w as _, resolution.h as _)
        .with_format(Format::Bgra8Srgb)
        .with_usage(ImageUsage::COLOR_ATTACHMENT | ImageUsage::SAMPLED);
    let image = gpu.create_image(&desc).context("Can't create image")?;

    let extent = Extent {
        width: resolution.w as _,
        height: resolution.h as _,
        depth: 1,
    };
    let framebuffer = unsafe {
        gpu.device()
            .create_framebuffer(&rpass, once(image.view_raw()), extent)
    }
    .context("Can't create framebuffer")?;

    Ok((image, framebuffer))
}

struct BlurPass<B: Backend> {
    render_pass: B::RenderPass,
    pipeline: B::GraphicsPipeline,
    desc_set: B::DescriptorSet,
    framebuffer: B::Framebuffer,
    image: Image<B>,
    is_last: bool,
}

impl<B: Backend> BlurPass<B> {
    fn new(
        gpu: &Gpu<B>,
        desc_pool: &mut B::DescriptorPool,
        desc_set_layout: &B::DescriptorSetLayout,
        pipeline_layout: &B::PipelineLayout,
        [vert, frag]: [&ShaderModule<B>; 2],
        is_last: bool,
        resolution: Extent2<u16>,
    ) -> Result<Self> {
        let attachment = Attachment {
            format: Some(Format::Bgra8Srgb),
            samples: 1,
            ops: AttachmentOps::new(AttachmentLoadOp::DontCare, AttachmentStoreOp::Store),
            stencil_ops: AttachmentOps::DONT_CARE,
            layouts: ImageLayout::Undefined..if is_last {
                ImageLayout::ShaderReadOnlyOptimal
            } else {
                ImageLayout::ColorAttachmentOptimal
            },
        };

        let subpass = SubpassDesc {
            colors: &[(0, ImageLayout::ColorAttachmentOptimal)],
            depth_stencil: None,
            inputs: &[],
            resolves: &[],
            preserves: &[],
        };

        let device = gpu.device();
        let render_pass = unsafe { device.create_render_pass(&[attachment], &[subpass], &[]) }
            .context("Can't create render pass")?;

        let desc_set = unsafe { desc_pool.allocate_set(desc_set_layout) }
            .context("Can't allocate descriptor set")?;

        let desc = basic_pipeline_desc(vert, frag, pipeline_layout, &render_pass);
        let pipeline = unsafe { device.create_graphics_pipeline(&desc, None) }
            .context("Can't create graphics pipeline")?;

        let (image, framebuffer) = create_image_and_framebuffer(gpu, &render_pass, resolution)?;

        Ok(BlurPass {
            render_pass,
            pipeline,
            desc_set,
            image,
            framebuffer,
            is_last,
        })
    }

    fn set_source(&mut self, gpu: &Gpu<B>, image: &Image<B>, sampler: &B::Sampler) {
        unsafe {
            gpu.device().write_descriptor_sets(vec![DescriptorSetWrite {
                set: &self.desc_set,
                binding: 0,
                array_offset: 0,
                descriptors: Some(Descriptor::CombinedImageSampler(
                    image.view_raw(),
                    ImageLayout::ShaderReadOnlyOptimal,
                    sampler,
                )),
            }]);
        }
    }

    fn resize(&mut self, ctx: &mut ResizeContext<'_, B>) -> Result<()> {
        let (new_image, new_fb) =
            create_image_and_framebuffer(ctx.gpu, &self.render_pass, ctx.resolution)?;
        let old_image = std::mem::replace(&mut self.image, new_image);
        ctx.cleaner.dispose_image(old_image);
        let old_fb = std::mem::replace(&mut self.framebuffer, new_fb);
        ctx.cleaner.dispose_framebuffer(old_fb);
        Ok(())
    }

    unsafe fn encode(
        &mut self,
        ctx: &mut FrameContext<'_, B>,
        layout: &B::PipelineLayout,
        radius: f32,
    ) {
        ctx.cbuf.begin_render_pass(
            &self.render_pass,
            &self.framebuffer,
            ctx.viewport().rect,
            &[],
            SubpassContents::Inline,
        );
        ctx.cbuf.bind_graphics_pipeline(&self.pipeline);
        ctx.cbuf
            .bind_graphics_descriptor_sets(layout, 0, once(&self.desc_set), &[]);
        let ref constants = if self.is_last {
            [0f32.to_bits(), radius.to_bits()]
        } else {
            [radius.to_bits(), 0f32.to_bits()]
        };
        ctx.cbuf
            .push_graphics_constants(layout, ShaderStageFlags::GRAPHICS, 24, constants);
        ctx.cbuf.draw(0..6, 0..1);
        ctx.cbuf.end_render_pass();
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.image.dispose(gpu);
        gpu.device().destroy_framebuffer(self.framebuffer);
        gpu.device().destroy_graphics_pipeline(self.pipeline);
        gpu.device().destroy_render_pass(self.render_pass);
    }
}

pub struct ViewData<B: Backend> {
    framebuffers: [B::Framebuffer; 2],
    pub images: [Image<B>; 2],
    resolution: [u16; 2], // TODO: replace with Extent2
}

pub struct BlurPipeline<B: Backend> {
    pipeline_layout: B::PipelineLayout,
    desc_pool: B::DescriptorPool,
    desc_set_layout: B::DescriptorSetLayout,
    sampler: B::Sampler,
    passes: [BlurPass<B>; 2],
}

impl<B: Backend> BlurPipeline<B> {
    pub fn new(gpu: &Gpu<B>, resolution: Extent2<u16>) -> Result<Self> {
        let mut desc_pool = unsafe {
            gpu.device().create_descriptor_pool(
                2,
                &[DescriptorRangeDesc {
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 2,
                }],
                gfx_hal::pso::DescriptorPoolCreateFlags::empty(),
            )
        }
        .context("Can't create descriptor pool")?;

        let desc_set_layout = unsafe {
            gpu.device().create_descriptor_set_layout(
                &[DescriptorSetLayoutBinding {
                    binding: 0,
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 1,
                    stage_flags: ShaderStageFlags::FRAGMENT,
                    immutable_samplers: false,
                }],
                &[],
            )
        }
        .context("Can't create descriptor set layout")?;

        let push_constants = &[(ShaderStageFlags::GRAPHICS, 0..32)];

        let pipeline_layout = unsafe {
            gpu.device()
                .create_pipeline_layout(once(&desc_set_layout), push_constants)
                .context("Can't create pipeline layout")?
        };

        let vert =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/blur.vert", ShaderKind::Vertex)
                .context("Can't compile vertex shader")?;
        let frag =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/blur.frag", ShaderKind::Fragment)
                .context("Can't compile fragment shader")?;
        let shaders = [&vert, &frag];

        let desc = SamplerDesc::new(Filter::Linear, WrapMode::Clamp);
        let sampler =
            unsafe { gpu.device().create_sampler(&desc) }.context("Can't create sampler")?;

        let mut make = |is_last| {
            BlurPass::new(
                gpu,
                &mut desc_pool,
                &desc_set_layout,
                &pipeline_layout,
                shaders,
                is_last,
                resolution,
            )
        };

        let pass0 = make(false)?;
        let mut pass1 = make(true)?;
        pass1.set_source(&gpu, &pass0.image, &sampler);

        unsafe {
            frag.dispose(gpu);
            vert.dispose(gpu);
        }

        Ok(BlurPipeline {
            pipeline_layout,
            desc_set_layout,
            desc_pool,
            sampler,
            passes: [pass0, pass1],
        })
    }

    pub fn resize(&mut self, ctx: &mut ResizeContext<'_, B>) -> Result<()> {
        self.passes[0].resize(ctx)?;
        self.passes[1].resize(ctx)?;
        self.passes[0].set_source(&ctx.gpu, &ctx.target, &self.sampler);
        let passes = &mut self.passes;
        let (pass0, pass1) = passes.split_first_mut().unwrap();
        pass0.set_source(&ctx.gpu, &ctx.target, &self.sampler);
        pass1[0].set_source(&ctx.gpu, &pass0.image, &self.sampler);
        Ok(())
    }

    pub fn execute(&mut self, ctx: &mut FrameContext<'_, B>, region: Aabr<u32>, radius: f32) {
        let viewport = ctx.viewport();

        let mut region = region.as_::<f32>();
        let res = ctx.resolution.as_();
        region.min /= res;
        region.max /= res;
        let region = region.into_rect();

        let range = SubresourceRange {
            aspects: Aspects::COLOR,
            levels: 0..1,
            layers: 0..1,
        };

        unsafe {
            ctx.cbuf.pipeline_barrier(
                PipelineStage::COLOR_ATTACHMENT_OUTPUT..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (
                        ImageAccess::COLOR_ATTACHMENT_WRITE,
                        ImageLayout::ColorAttachmentOptimal,
                    )
                        ..(ImageAccess::SHADER_READ, ImageLayout::ShaderReadOnlyOptimal),
                    target: ctx.target.raw(),
                    range: range.clone(),
                    families: None,
                }],
            );

            ctx.cbuf.set_viewports(0, &[viewport.clone()]);
            ctx.cbuf.set_scissors(0, &[viewport.rect]);
            ctx.cbuf.push_graphics_constants(
                &self.pipeline_layout,
                ShaderStageFlags::GRAPHICS,
                0,
                &[
                    region.x.to_bits(),
                    region.y.to_bits(),
                    region.w.to_bits(),
                    region.h.to_bits(),
                    (ctx.resolution.w as f32).to_bits(),
                    (ctx.resolution.h as f32).to_bits(),
                ],
            );

            self.passes[0].encode(ctx, &self.pipeline_layout, radius);

            ctx.cbuf.pipeline_barrier(
                PipelineStage::COLOR_ATTACHMENT_OUTPUT..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (
                        ImageAccess::COLOR_ATTACHMENT_WRITE,
                        ImageLayout::ColorAttachmentOptimal,
                    )
                        ..(ImageAccess::SHADER_READ, ImageLayout::ShaderReadOnlyOptimal),
                    target: self.passes[0].image.raw(),
                    range: range.clone(),
                    families: None,
                }],
            );

            self.passes[1].encode(ctx, &self.pipeline_layout, radius);

            ctx.cbuf.pipeline_barrier(
                PipelineStage::FRAGMENT_SHADER..PipelineStage::COLOR_ATTACHMENT_OUTPUT,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (ImageAccess::SHADER_READ, ImageLayout::ShaderReadOnlyOptimal)
                        ..(
                            ImageAccess::COLOR_ATTACHMENT_WRITE,
                            ImageLayout::ColorAttachmentOptimal,
                        ),
                    target: ctx.target.raw(),
                    range,
                    families: None,
                }],
            );
        }
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        let [pass0, pass1] = self.passes;
        pass0.dispose(gpu);
        pass1.dispose(gpu);
        gpu.device().destroy_pipeline_layout(self.pipeline_layout);
        gpu.device().destroy_descriptor_pool(self.desc_pool);
        gpu.device()
            .destroy_descriptor_set_layout(self.desc_set_layout);
    }
}
