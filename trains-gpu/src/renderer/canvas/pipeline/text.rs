use std::iter::once;

use gfx_hal::{
    buffer::SubRange,
    command::CommandBuffer,
    device::Device,
    format::Format,
    image::{Filter, SamplerDesc, WrapMode},
    pso::{
        AttributeDesc, Descriptor, DescriptorPool, DescriptorRangeDesc, DescriptorSetLayoutBinding,
        DescriptorSetWrite, DescriptorType, Element, ImageDescriptorType, PipelineStage,
        ShaderStageFlags, VertexBufferDesc, VertexInputRate,
    },
    Backend,
};

use anyhow::{Context, Result};
use rusttype::{Font, Point, PositionedGlyph, Scale};
use vek::{Aabr, Extent2, Mat3, Rgba, Vec2};

use crate::{
    buffer::{Buffer, BufferAccess, BufferUsage, DynamicBuffer, Vertex},
    canvas::Text,
    command::{TransferEncoder, UploadCommand},
    gpu::Gpu,
    image::{ImageAccess, ImageLayout, ImageUsage, TextureCache},
    memory::MemoryUsage,
    renderer::{
        canvas::vertex::UiVertex,
        shader::{ShaderKind, ShaderModule},
        util::basic_pipeline_desc,
        FrameContext,
    },
    util::Pod,
};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct Instance {
    transform_col0_1: [f32; 4],
    transform_col2: [f32; 2],
    color: [f32; 4],
    uv_rect: [f32; 4],
}

impl Instance {
    pub fn new(
        proj: Mat3<f32>,
        color: Rgba<f32>,
        uv_rect: Aabr<u32>,
        atlas_size: Extent2<u32>,
        glyph: &PositionedGlyph<'_>,
    ) -> Instance {
        let bb = glyph.pixel_bounding_box().unwrap();
        let width = uv_rect.into_rect().w as f32;
        let height = uv_rect.into_rect().h as f32;
        let model = Mat3::scaling_3d((width, height, 1.0))
            .translated_2d((bb.min.x as f32, bb.min.y as f32));

        let transform = proj * model;

        let tr_col0 = transform.cols.x;
        let tr_col1 = transform.cols.y;
        let tr_col2 = transform.cols.z;

        let mut r = uv_rect.as_();
        r.min /= atlas_size.as_();
        r.max /= atlas_size.as_();
        let r = r.into_rect();
        Instance {
            transform_col0_1: [tr_col0.x, tr_col0.y, tr_col1.x, tr_col1.y],
            transform_col2: [tr_col2.x, tr_col2.y],
            color: color.into_array(),
            uv_rect: [r.x, r.y, r.w, r.h],
        }
    }
}

unsafe impl Pod for Instance {}

impl Vertex for Instance {
    fn vbuf_desc() -> VertexBufferDesc {
        VertexBufferDesc {
            binding: 1,
            stride: 56,
            rate: VertexInputRate::Instance(1),
        }
    }

    fn attributes() -> Vec<AttributeDesc> {
        let mut attrs = (0..4)
            .map(|i| AttributeDesc {
                location: i,
                binding: 0,
                element: Element {
                    format: Format::Rgba32Sfloat,
                    offset: 0,
                },
            })
            .collect::<Vec<_>>();
        attrs[1].element.offset = 16;
        attrs[1].element.format = Format::Rg32Sfloat;
        attrs[2].element.offset = 24;
        attrs[3].element.offset = 40;
        attrs
    }
}

#[derive(Eq, Hash, PartialEq)]
struct GlyphKey {
    id: u16,
    font: u32,
    size: Extent2<u16>,
    offset: Vec2<u8>,
}

impl GlyphKey {
    pub fn new<'a>(font: usize, glyph: &mut PositionedGlyph<'a>) -> Option<Self> {
        let offset = Vec2::new(glyph.position().x.fract(), glyph.position().y.fract());

        glyph.set_position(Point {
            x: offset.x,
            y: offset.y,
        });

        let bb = glyph.pixel_bounding_box()?;

        Some(GlyphKey {
            id: glyph.id().0,
            font: font as _,
            size: Extent2::new(bb.width() as _, bb.height() as _),
            offset: (offset * 255.0).numcast()?,
        })
    }
}

struct GlyphEntry {
    is_used: bool,
}

fn layout_text<'a>(
    font: &'a Font,
    text: &'a Text,
) -> impl Iterator<Item = PositionedGlyph<'a>> + 'a {
    font.layout(
        &text.text,
        Scale {
            x: text.font_size.x,
            y: text.font_size.y,
        },
        Point {
            x: text.origin.x,
            y: text.origin.y,
        },
    )
}

pub fn num_glyphs(text: &Text, fonts: &[Font]) -> usize {
    layout_text(&fonts[text.font_id], text)
        .filter(|gl| gl.pixel_bounding_box().is_some())
        .count()
}

pub struct TextPipeline<B: Backend> {
    pipeline: B::GraphicsPipeline,
    pipeline_layout: B::PipelineLayout,
    vertex_buffer: Buffer<B, UiVertex>,
    instance_buffer: DynamicBuffer<B, Instance>,
    atlas: TextureCache<B, GlyphKey, GlyphEntry>,
    desc_pool: B::DescriptorPool,
    desc_set: B::DescriptorSet,
    desc_set_layout: B::DescriptorSetLayout,
    sampler: B::Sampler,
}

impl<B: Backend> TextPipeline<B> {
    pub fn new(
        gpu: &Gpu<B>,
        encoder: &mut TransferEncoder<B>,
        render_pass: &B::RenderPass,
    ) -> Result<Self> {
        let device = gpu.device();
        let desc_set_layout = unsafe {
            device.create_descriptor_set_layout(
                &[DescriptorSetLayoutBinding {
                    binding: 0,
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 1,
                    stage_flags: ShaderStageFlags::FRAGMENT,
                    immutable_samplers: false,
                }],
                &[],
            )
        }
        .context("Can't create descriptor set layout")?;

        let pipeline_layout = unsafe {
            gpu.device()
                .create_pipeline_layout(Some(&desc_set_layout), &[])
                .context("Can't create pipeline layout")?
        };

        let vert_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/text.vert", ShaderKind::Vertex)
                .context("Can't compile vertex shader")?;
        let frag_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/text.frag", ShaderKind::Fragment)
                .context("Can't compile fragment shader")?;

        let mut desc =
            basic_pipeline_desc(&vert_shader, &frag_shader, &pipeline_layout, render_pass);

        UiVertex::add_to_pipeline(&mut desc);
        Instance::add_to_pipeline(&mut desc);

        let pipeline = unsafe {
            gpu.device()
                .create_graphics_pipeline(&desc, None)
                .context("Can't create graphics pipeline")?
        };

        unsafe {
            frag_shader.dispose(gpu);
            vert_shader.dispose(gpu);
        }

        let vertex_buffer = gpu.create_buffer(
            UiVertex::RECT.len(),
            BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            MemoryUsage::GpuOnly,
        )?;

        unsafe {
            encoder.upload_buffer(
                gpu,
                UploadCommand {
                    buffer: &vertex_buffer,
                    data: UiVertex::RECT,
                    start_state: None,
                    end_state: (
                        PipelineStage::VERTEX_INPUT,
                        BufferAccess::VERTEX_BUFFER_READ,
                    ),
                },
            )?;
        }

        let instance_buffer = DynamicBuffer::new(BufferUsage::VERTEX | BufferUsage::TRANSFER_DST);

        let atlas = TextureCache::new(
            gpu,
            (64, 64),
            Format::R8Unorm,
            ImageUsage::TRANSFER_DST | ImageUsage::TRANSFER_SRC | ImageUsage::SAMPLED,
        )?;

        let mut desc_pool = unsafe {
            device.create_descriptor_pool(
                1,
                &[DescriptorRangeDesc {
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled { with_sampler: true },
                    },
                    count: 1,
                }],
                gfx_hal::pso::DescriptorPoolCreateFlags::empty(),
            )
        }
        .context("Can't create descriptor pool")?;

        let desc_set = unsafe { desc_pool.allocate_set(&desc_set_layout) }
            .context("Can't allocate descriptor set")?;

        let sampler =
            unsafe { device.create_sampler(&SamplerDesc::new(Filter::Linear, WrapMode::Clamp)) }
                .context("Can't create sampler")?;

        unsafe {
            device.write_descriptor_sets(vec![DescriptorSetWrite {
                set: &desc_set,
                binding: 0,
                array_offset: 0,
                descriptors: Some(Descriptor::CombinedImageSampler(
                    atlas.view_raw(),
                    ImageLayout::ShaderReadOnlyOptimal,
                    &sampler,
                )),
            }]);
        }

        Ok(TextPipeline {
            pipeline,
            pipeline_layout,
            vertex_buffer,
            instance_buffer,
            atlas,
            desc_pool,
            desc_set,
            desc_set_layout,
            sampler,
        })
    }

    fn upload_glyphs<'a>(
        &mut self,
        ctx: &mut FrameContext<'_, B>,
        texts: impl Iterator<Item = &'a Text>,
        fonts: &[Font],
    ) -> Result<()> {
        for text in texts {
            let font = &fonts[text.font_id];
            let glyphs = layout_text(font, text);

            glyphs
                .flat_map(|mut glyph| {
                    let key = GlyphKey::new(text.font_id, &mut glyph)?;
                    Some((glyph, key))
                })
                .map(|(glyph, key)| {
                    if let Some(entry) = self.atlas.get_mut(&key) {
                        entry.is_used = true;
                        return Ok(());
                    }

                    // rasterize glyph
                    let size = key.size.as_();
                    let mut data = vec![0; size.product()];
                    glyph.draw(|x, y, v| {
                        data[(y as usize) * size.w + (x as usize)] = (v * 255.0) as u8;
                    });
                    let size = size.as_().into_tuple();
                    let value = GlyphEntry { is_used: true };
                    self.atlas
                        .add(ctx.gpu, ctx.cleaner, key, value, size, &data)
                })
                .collect::<Result<_>>()?;
        }

        Ok(())
    }

    pub fn prepare<'a>(
        &mut self,
        ctx: &mut FrameContext<'_, B>,
        texts: impl Iterator<Item = &'a Text> + Clone,
        fonts: &[Font],
    ) -> Result<()> {
        let proj = ctx.proj_2d();

        for (_, glyph) in self.atlas.iter_mut() {
            glyph.is_used = false;
        }

        self.upload_glyphs(ctx, texts.clone(), fonts)?;

        self.atlas.retain(|_, glyph| glyph.is_used);

        let init_state = (
            PipelineStage::TOP_OF_PIPE,
            ImageAccess::empty(),
            ImageLayout::Undefined,
        );

        let state = (
            PipelineStage::FRAGMENT_SHADER,
            ImageAccess::SHADER_READ,
            ImageLayout::ShaderReadOnlyOptimal,
        );

        let transition = if ctx.frame.index() == 0 {
            init_state..state
        } else {
            state..state
        };

        let resized = self
            .atlas
            .flush(ctx.gpu, ctx.cleaner, ctx.cbuf, transition)?;

        if resized {
            unsafe {
                ctx.gpu
                    .device()
                    .write_descriptor_sets(vec![DescriptorSetWrite {
                        set: &self.desc_set,
                        binding: 0,
                        array_offset: 0,
                        descriptors: Some(Descriptor::CombinedImageSampler(
                            self.atlas.view_raw(),
                            ImageLayout::ShaderReadOnlyOptimal,
                            &self.sampler,
                        )),
                    }]);
            }
        }

        let atlas = &self.atlas;
        let instances = texts.flat_map(|t| {
            layout_text(&fonts[t.font_id], t).flat_map(move |glyph| {
                let key = GlyphKey::new(t.font_id, &mut glyph.clone())?;
                let mut uv_rect = atlas.get_rect(&key)?;
                uv_rect.max = uv_rect.min + key.size.as_();
                Some(Instance::new(proj, t.color, uv_rect, atlas.size(), &glyph))
            })
        });

        self.instance_buffer.clear();
        self.instance_buffer
            .extend(ctx.gpu, ctx.cleaner, instances)?;

        unsafe {
            let state = (
                PipelineStage::VERTEX_INPUT,
                BufferAccess::VERTEX_BUFFER_READ,
            );

            self.instance_buffer
                .upload(ctx.gpu, ctx.cleaner, ctx.cbuf, state..state)
        }
    }

    pub unsafe fn encode(&mut self, cbuf: &mut B::CommandBuffer, start: u32, end: u32) {
        cbuf.bind_vertex_buffers(
            0,
            once((self.vertex_buffer.raw(), SubRange::WHOLE))
                .chain(once((self.instance_buffer.raw(), SubRange::WHOLE))),
        );
        cbuf.bind_graphics_descriptor_sets(&self.pipeline_layout, 0, Some(&self.desc_set), &[]);
        cbuf.bind_graphics_pipeline(&self.pipeline);
        cbuf.draw(0..6, start..end);
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        gpu.device().destroy_pipeline_layout(self.pipeline_layout);
        gpu.device().destroy_graphics_pipeline(self.pipeline);
        gpu.device().destroy_descriptor_pool(self.desc_pool);
        gpu.device()
            .destroy_descriptor_set_layout(self.desc_set_layout);
        self.atlas.dispose(gpu);
        gpu.device().destroy_sampler(self.sampler);
        self.vertex_buffer.dispose(gpu);
        self.instance_buffer.dispose(gpu);
    }
}
