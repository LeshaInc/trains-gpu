use std::iter::once;

use gfx_hal::{
    buffer::SubRange,
    command::CommandBuffer,
    device::Device,
    format::Format,
    pso::{AttributeDesc, Element, PipelineStage, VertexBufferDesc, VertexInputRate},
    Backend,
};

use anyhow::{Context, Result};
use vek::{Aabr, Mat3};

use crate::{
    buffer::{Buffer, BufferAccess, BufferUsage, DynamicBuffer, Vertex},
    canvas::Rect,
    command::{TransferEncoder, UploadCommand},
    gpu::Gpu,
    memory::MemoryUsage,
    renderer::{
        canvas::vertex::UiVertex,
        shader::{ShaderKind, ShaderModule},
        util::basic_pipeline_desc,
        FrameContext,
    },
    util::Pod,
};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct Instance {
    transform_col0_1: [f32; 4],
    transform_col2_shadow_offset: [f32; 4],
    color: [f32; 4],
    shadow_color: [f32; 4],
    border_color: [f32; 4],
    border_radii: [f32; 4],
    size_blur_radius_thickness: [f32; 4],
}

impl Instance {
    pub fn new(proj: Mat3<f32>, rect: &Rect) -> Instance {
        let aabr = Aabr {
            min: rect.origin,
            max: rect.origin + rect.size,
        };

        let shadow_aabr = Aabr {
            min: rect.origin + rect.shadow_offset - rect.shadow_blur_radius,
            max: rect.origin + rect.size + rect.shadow_offset + rect.shadow_blur_radius,
        };

        let combined = aabr.union(shadow_aabr);

        let model = Mat3::scaling_3d((combined.size().w, combined.size().h, 1.0))
            .translated_2d(combined.min);

        let transform = proj * model;

        let tr_col0 = transform.cols.x;
        let tr_col1 = transform.cols.y;
        let tr_col2 = transform.cols.z;

        Instance {
            transform_col0_1: [tr_col0.x, tr_col0.y, tr_col1.x, tr_col1.y],
            transform_col2_shadow_offset: [
                tr_col2.x,
                tr_col2.y,
                rect.shadow_offset.x,
                rect.shadow_offset.y,
            ],
            color: rect.color.into_array(),
            shadow_color: rect.shadow_color.into_array(),
            border_color: rect.border_color.into_array(),
            border_radii: rect.border_radii.into(),
            size_blur_radius_thickness: [
                rect.size.x,
                rect.size.y,
                rect.shadow_blur_radius,
                rect.border_thickness,
            ],
        }
    }
}

unsafe impl Pod for Instance {}

impl Vertex for Instance {
    fn vbuf_desc() -> VertexBufferDesc {
        VertexBufferDesc {
            binding: 1,
            stride: 112,
            rate: VertexInputRate::Instance(1),
        }
    }

    fn attributes() -> Vec<AttributeDesc> {
        (0..7)
            .map(|i| AttributeDesc {
                location: i,
                binding: 0,
                element: Element {
                    format: Format::Rgba32Sfloat,
                    offset: i * 16,
                },
            })
            .collect()
    }
}

pub struct RectsPipeline<B: Backend> {
    pipeline: B::GraphicsPipeline,
    pipeline_layout: B::PipelineLayout,
    vertex_buffer: Buffer<B, UiVertex>,
    instance_buffer: DynamicBuffer<B, Instance>,
}

impl<B: Backend> RectsPipeline<B> {
    pub fn new(
        gpu: &Gpu<B>,
        encoder: &mut TransferEncoder<B>,
        render_pass: &B::RenderPass,
    ) -> Result<Self> {
        let pipeline_layout = unsafe {
            gpu.device()
                .create_pipeline_layout(&[], &[])
                .context("Can't create pipeline layout")?
        };

        let vert_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/rects.vert", ShaderKind::Vertex)
                .context("Can't compile vertex shader")?;
        let frag_shader =
            ShaderModule::<B>::compile(gpu.device(), "shaders/ui/rects.frag", ShaderKind::Fragment)
                .context("Can't compile fragment shader")?;

        let mut desc =
            basic_pipeline_desc(&vert_shader, &frag_shader, &pipeline_layout, render_pass);

        UiVertex::add_to_pipeline(&mut desc);
        Instance::add_to_pipeline(&mut desc);

        let pipeline = unsafe {
            gpu.device()
                .create_graphics_pipeline(&desc, None)
                .context("Can't create graphics pipeline")?
        };

        unsafe {
            frag_shader.dispose(gpu);
            vert_shader.dispose(gpu);
        }

        let vertex_buffer = gpu.create_buffer(
            UiVertex::RECT.len(),
            BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            MemoryUsage::GpuOnly,
        )?;

        unsafe {
            encoder.upload_buffer(
                gpu,
                UploadCommand {
                    buffer: &vertex_buffer,
                    data: UiVertex::RECT,
                    start_state: None,
                    end_state: (
                        PipelineStage::VERTEX_INPUT,
                        BufferAccess::VERTEX_BUFFER_READ,
                    ),
                },
            )?;
        }

        let instance_buffer = DynamicBuffer::new(BufferUsage::VERTEX | BufferUsage::TRANSFER_DST);

        Ok(RectsPipeline {
            pipeline,
            pipeline_layout,
            vertex_buffer,
            instance_buffer,
        })
    }

    pub fn prepare<'a>(
        &mut self,
        ctx: &mut FrameContext<'_, B>,
        rects: impl Iterator<Item = &'a Rect> + 'a,
    ) -> Result<()> {
        let proj = ctx.proj_2d();

        let instances = rects.map(|r| Instance::new(proj, &r));

        self.instance_buffer.clear();
        self.instance_buffer
            .extend(ctx.gpu, ctx.cleaner, instances)?;

        unsafe {
            let state = (
                PipelineStage::VERTEX_INPUT,
                BufferAccess::VERTEX_BUFFER_READ,
            );

            self.instance_buffer
                .upload(ctx.gpu, ctx.cleaner, ctx.cbuf, state..state)?;
        }

        Ok(())
    }

    pub unsafe fn encode(&mut self, cbuf: &mut B::CommandBuffer, start: u32, end: u32) {
        cbuf.bind_vertex_buffers(
            0,
            once((self.vertex_buffer.raw(), SubRange::WHOLE))
                .chain(once((self.instance_buffer.raw(), SubRange::WHOLE))),
        );
        cbuf.bind_graphics_pipeline(&self.pipeline);
        cbuf.draw(0..6, start..end);
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        gpu.device().destroy_pipeline_layout(self.pipeline_layout);
        gpu.device().destroy_graphics_pipeline(self.pipeline);
        self.vertex_buffer.dispose(gpu);
        self.instance_buffer.dispose(gpu);
    }
}
