mod instance;
mod pipeline;
mod vertex;

use gfx_hal::{
    command::{AttachmentClear, ClearColor, CommandBuffer, SubpassContents},
    device::Device,
    format::Format,
    image::{Extent, Layout},
    pass::{Attachment, AttachmentOps, SubpassDesc},
    pso::ClearRect,
    Backend,
};

use anyhow::{Context, Result};
use vek::Extent2;

use crate::{
    canvas::{BlurCommand, Command, DrawCommand, DrawList},
    command::TransferEncoder,
    gpu::Gpu,
    image::Image,
    renderer::{FrameContext, ResizeContext},
};

use self::pipeline::{num_glyphs, BlurPipeline, RectsPipeline, TextPipeline};

pub struct CanvasRenderer<B: Backend> {
    rects_pipeline: RectsPipeline<B>,
    text_pipeline: TextPipeline<B>,
    blur_pipeline: BlurPipeline<B>,
    render_pass: B::RenderPass,
    framebuffer: B::Framebuffer,
}

fn create_framebuffer<B: Backend>(
    device: &B::Device,
    rpass: &B::RenderPass,
    image: &Image<B>,
    resolution: Extent2<u16>,
) -> Result<B::Framebuffer> {
    let extent = Extent {
        width: resolution.w as _,
        height: resolution.h as _,
        depth: 1,
    };

    unsafe { device.create_framebuffer(&rpass, Some(image.view_raw()), extent) }
        .context("Can't create framebuffer")
}

impl<B: Backend> CanvasRenderer<B> {
    pub fn new(
        gpu: &Gpu<B>,
        encoder: &mut TransferEncoder<B>,
        target: &Image<B>,
        resolution: Extent2<u16>,
    ) -> Result<Self> {
        let attachment = Attachment {
            format: Some(Format::Bgra8Srgb),
            samples: 1,
            ops: AttachmentOps::PRESERVE,
            stencil_ops: AttachmentOps::DONT_CARE,
            layouts: Layout::ColorAttachmentOptimal..Layout::ColorAttachmentOptimal,
        };

        let subpass = SubpassDesc {
            colors: &[(0, Layout::ColorAttachmentOptimal)],
            depth_stencil: None,
            inputs: &[],
            resolves: &[],
            preserves: &[],
        };

        let render_pass = unsafe {
            gpu.device()
                .create_render_pass(&[attachment], &[subpass], &[])
                .context("Can't create render pass")?
        };

        let rects_pipeline = RectsPipeline::new(gpu, encoder, &render_pass)?;
        let text_pipeline = TextPipeline::new(gpu, encoder, &render_pass)?;
        let blur_pipeline = BlurPipeline::new(gpu, resolution)?;

        Ok(CanvasRenderer {
            rects_pipeline,
            text_pipeline,
            blur_pipeline,
            framebuffer: create_framebuffer(gpu.device(), &render_pass, target, resolution)?,
            render_pass,
        })
    }

    pub fn resize(&mut self, ctx: &mut ResizeContext<'_, B>) -> Result<()> {
        self.blur_pipeline.resize(ctx)?;

        let new_fb = unsafe {
            ctx.gpu.device().create_framebuffer(
                &self.render_pass,
                Some(ctx.target.view_raw()),
                ctx.image_extent(),
            )?
        };

        let old_fb = std::mem::replace(&mut self.framebuffer, new_fb);
        ctx.cleaner.dispose_framebuffer(old_fb);

        Ok(())
    }

    fn prepare_draws(&mut self, ctx: &mut FrameContext<'_, B>, list: &DrawList) -> Result<()> {
        self.rects_pipeline.prepare(
            ctx,
            list.commands.iter().filter_map(|cmd| match cmd {
                Command::Draw(DrawCommand::Rect(r)) => Some(r),
                _ => None,
            }),
        )?;

        self.text_pipeline.prepare(
            ctx,
            list.commands.iter().filter_map(|cmd| match cmd {
                Command::Draw(DrawCommand::Text(t)) => Some(t),
                _ => None,
            }),
            &list.fonts,
        )?;

        Ok(())
    }

    fn encode_draws(
        &mut self,
        ctx: &mut FrameContext<'_, B>,
        list: &DrawList,
        start: usize,
        end: usize,
        state: &mut [u32; 4],
        clear: bool,
    ) {
        let viewport = ctx.viewport();
        let [mut rects_offset, mut rects_count, mut text_offset, mut text_count] = state;
        let mut commands = list.commands[start..end].iter();

        unsafe {
            ctx.cbuf.set_viewports(0, &[viewport.clone()]);
            ctx.cbuf.set_scissors(0, &[viewport.rect]);
            ctx.cbuf.begin_render_pass(
                &self.render_pass,
                &self.framebuffer,
                viewport.rect,
                &[],
                SubpassContents::Inline,
            );

            if clear {
                ctx.cbuf.clear_attachments(
                    &[AttachmentClear::Color {
                        index: 0,
                        value: ClearColor { float32: [1.0; 4] },
                    }],
                    &[ClearRect {
                        rect: ctx.pso_rect(),
                        layers: 0..1,
                    }],
                );
            }

            let rects = &mut self.rects_pipeline;
            let mut flush_rects = |ctx: &mut FrameContext<'_, B>, count| {
                rects.encode(ctx.cbuf, rects_offset, rects_offset + count);
                rects_offset += count;
            };

            let texts = &mut self.text_pipeline;
            let mut flush_texts = |ctx: &mut FrameContext<'_, B>, count| {
                texts.encode(ctx.cbuf, text_offset, text_offset + count);
                text_offset += count;
            };

            loop {
                match commands.next() {
                    Some(Command::Draw(DrawCommand::Rect(_))) => {
                        if text_count > 0 {
                            flush_texts(ctx, text_count);
                            text_count = 0;
                        }
                        rects_count += 1;
                    }
                    Some(Command::Draw(DrawCommand::Text(text))) => {
                        if rects_count > 0 {
                            flush_rects(ctx, rects_count);
                            rects_count = 0;
                        }
                        text_count += num_glyphs(text, &list.fonts) as u32;
                    }
                    Some(Command::Blur(_)) => panic!("unexpected blur command"),
                    _ => {
                        if rects_count > 0 {
                            flush_rects(ctx, rects_count);
                        }
                        if text_count > 0 {
                            flush_texts(ctx, text_count);
                        }
                        break;
                    }
                }
            }

            ctx.cbuf.end_render_pass();
        }

        *state = [rects_offset, rects_count, text_offset, text_count];
    }

    fn encode_blur(&mut self, ctx: &mut FrameContext<'_, B>, blur: &BlurCommand) {
        self.blur_pipeline.execute(ctx, blur.region, blur.radius);
    }

    pub fn encode(&mut self, ctx: &mut FrameContext<'_, B>, list: &DrawList) -> Result<()> {
        self.prepare_draws(ctx, list)?;

        let mut state = [0; 4];
        let mut start = 0;
        for (i, cmd) in list.commands.iter().enumerate() {
            if let Command::Blur(blur) = cmd {
                self.encode_draws(ctx, list, start, i, &mut state, start == 0);
                start = i + 1;
                self.encode_blur(ctx, blur)
            }
        }

        if start < list.commands.len() {
            self.encode_draws(
                ctx,
                list,
                start,
                list.commands.len(),
                &mut state,
                start == 0,
            );
        }

        Ok(())
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.rects_pipeline.dispose(gpu);
        self.text_pipeline.dispose(gpu);
        gpu.device().destroy_render_pass(self.render_pass);
        self.blur_pipeline.dispose(gpu);
    }
}
