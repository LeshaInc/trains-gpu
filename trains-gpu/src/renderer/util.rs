use gfx_hal::{pass::Subpass, pso, Backend};

use super::shader::ShaderModule;

pub fn basic_pipeline_desc<'a, B: Backend>(
    vert_shader: &'a ShaderModule<B>,
    frag_shader: &'a ShaderModule<B>,
    layout: &'a B::PipelineLayout,
    pass: &'a B::RenderPass,
) -> pso::GraphicsPipelineDesc<'a, B> {
    pso::GraphicsPipelineDesc::<B> {
        shaders: pso::GraphicsShaderSet {
            vertex: pso::EntryPoint {
                entry: "main",
                module: &vert_shader.raw,
                specialization: pso::Specialization::EMPTY,
            },
            fragment: Some(pso::EntryPoint {
                entry: "main",
                module: &frag_shader.raw,
                specialization: pso::Specialization::EMPTY,
            }),
            hull: None,
            domain: None,
            geometry: None,
        },
        rasterizer: pso::Rasterizer::FILL,
        vertex_buffers: vec![],
        attributes: vec![],
        input_assembler: pso::InputAssemblerDesc::new(pso::Primitive::TriangleList),
        blender: pso::BlendDesc {
            logic_op: None,
            targets: vec![pso::ColorBlendDesc {
                mask: pso::ColorMask::ALL,
                blend: Some(pso::BlendState::ALPHA),
            }],
        },
        depth_stencil: pso::DepthStencilDesc::default(),
        multisampling: None,
        baked_states: pso::BakedStates::default(),
        layout: &layout,
        subpass: Subpass {
            index: 0,
            main_pass: &pass,
        },
        flags: pso::PipelineCreationFlags::empty(),
        parent: pso::BasePipeline::None,
    }
}
