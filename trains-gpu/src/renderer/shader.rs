#![allow(unused_imports)]

use std::{fs, path::Path};

use gfx_hal::{device::Device, Backend};

use anyhow::{anyhow, bail, Result};

#[cfg(feature = "shaderc")]
use shaderc::{CompilationArtifact, CompileOptions, Compiler, OptimizationLevel};

use crate::{gpu::Gpu, util::Relevant};

#[derive(Clone, Copy, Debug)]
pub enum ShaderKind {
    Vertex,
    Hull,
    Domain,
    Geometry,
    Fragment,
    Compute,
}

#[cfg(feature = "shaderc")]
impl Into<shaderc::ShaderKind> for ShaderKind {
    fn into(self) -> shaderc::ShaderKind {
        match self {
            ShaderKind::Vertex => shaderc::ShaderKind::Vertex,
            ShaderKind::Hull => shaderc::ShaderKind::TessControl,
            ShaderKind::Domain => shaderc::ShaderKind::TessEvaluation,
            ShaderKind::Geometry => shaderc::ShaderKind::Geometry,
            ShaderKind::Fragment => shaderc::ShaderKind::Fragment,
            ShaderKind::Compute => shaderc::ShaderKind::Compute,
        }
    }
}

#[cfg(feature = "shaderc")]
fn compile_shader(source: String, name: String, kind: ShaderKind) -> Result<CompilationArtifact> {
    let mut compiler = Compiler::new().ok_or_else(|| anyhow!("Cannot initialize GLSL compiler"))?;
    let mut options =
        CompileOptions::new().ok_or_else(|| anyhow!("Cannot initialize GLSL compiler"))?;
    options.set_optimization_level(OptimizationLevel::Performance);
    Ok(compiler.compile_into_spirv(&source, kind.into(), &name, "main", Some(&options))?)
}

#[derive(Debug)]
pub struct ShaderModule<B: Backend> {
    pub raw: B::ShaderModule,
    relevant: Relevant<Self>,
}

impl<B: Backend> ShaderModule<B> {
    pub fn new_from_bytes_u32(device: &B::Device, spirv: &[u32]) -> Result<Self> {
        let module = unsafe { device.create_shader_module(spirv) }?;

        Ok(ShaderModule {
            raw: module,
            relevant: Relevant::new(),
        })
    }

    #[cfg(feature = "shaderc")]
    pub fn compile(device: &B::Device, path: impl AsRef<Path>, kind: ShaderKind) -> Result<Self> {
        let path = path.as_ref();
        log::trace!("Compiling shader {}", path.display());
        let source = fs::read_to_string(path)?;
        let name = path.display().to_string();
        let artifact = compile_shader(source, name, kind)?;
        Self::new_from_bytes_u32(device, artifact.as_binary())
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.relevant.dispose();
        gpu.device().destroy_shader_module(self.raw);
    }
}
