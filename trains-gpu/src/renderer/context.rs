use gfx_hal::{image::Extent, pso, Backend};

use vek::{Extent2, Mat3};

use crate::{cleaner::FrameCleaner, gpu::Gpu, image::Image, util::Frame};

fn res_to_extent(res: Extent2<u16>) -> Extent {
    Extent {
        width: res.w as _,
        height: res.h as _,
        depth: 1,
    }
}

pub struct FrameContext<'a, B: Backend> {
    pub gpu: &'a Gpu<B>,
    pub cbuf: &'a mut B::CommandBuffer,
    pub cleaner: &'a mut FrameCleaner<B>,
    pub target: &'a Image<B>,
    pub window_target: &'a B::ImageView,
    pub resolution: Extent2<u16>,
    pub frame: Frame,
}

impl<B: Backend> FrameContext<'_, B> {
    pub fn viewport(&self) -> pso::Viewport {
        pso::Viewport {
            rect: self.pso_rect(),
            depth: 0.0..1.0,
        }
    }

    pub fn pso_rect(&self) -> pso::Rect {
        pso::Rect {
            x: 0,
            y: 0,
            w: self.resolution.w as _,
            h: self.resolution.h as _,
        }
    }

    pub fn image_extent(&self) -> Extent {
        res_to_extent(self.resolution)
    }

    pub fn proj_2d(&self) -> Mat3<f32> {
        Mat3::scaling_3d((
            2.0 / self.resolution.w as f32,
            2.0 / self.resolution.h as f32,
            1.0,
        ))
        .translated_2d((-1.0, -1.0))
    }
}

pub struct ResizeContext<'a, B: Backend> {
    pub gpu: &'a Gpu<B>,
    pub cleaner: &'a mut FrameCleaner<B>,
    pub target: &'a Image<B>,
    pub resolution: Extent2<u16>,
}

impl<B: Backend> ResizeContext<'_, B> {
    pub fn image_extent(&self) -> Extent {
        res_to_extent(self.resolution)
    }
}
