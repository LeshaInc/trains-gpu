use gfx_hal::format::Format;
use gfx_hal::pso::{AttributeDesc, Element, VertexBufferDesc, VertexInputRate};

use crate::Pod;

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub texture: [f32; 2],
}

unsafe impl Pod for Vertex {}

impl Vertex {
    pub const VBUF_DESC: VertexBufferDesc = VertexBufferDesc {
        binding: 0,
        stride: 32,
        rate: VertexInputRate::Vertex,
    };

    pub const ATTRS_DESC: &'static [AttributeDesc] = &[
        AttributeDesc {
            location: 0,
            binding: 0,
            element: Element {
                format: Format::Rgb32Sfloat,
                offset: 0,
            },
        },
        AttributeDesc {
            location: 1,
            binding: 0,
            element: Element {
                format: Format::Rgb32Sfloat,
                offset: 12,
            },
        },
        AttributeDesc {
            location: 2,
            binding: 0,
            element: Element {
                format: Format::Rg32Sfloat,
                offset: 24,
            },
        },
    ];
}
