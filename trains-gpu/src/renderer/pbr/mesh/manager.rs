use std::sync::Arc;

use gfx_hal::Backend;
use gfx_hal::device::OutOfMemory;

use super::MeshData;
use crate::transfer::{TransferEncoder, TransferManager};
use crate::Gpu;

pub struct MeshManager<B: Backend> {
    transfer: TransferManager<B>,
}

impl<B: Backend> MeshManager<B> {
    pub fn new(gpu: Arc<Gpu<B>>) -> Result<Self, OutOfMemory> {
        Ok(Self {
            transfer: TransferManager::new(&gpu)?,
            gpu,
        })
    }

    pub fn new_uploader(&mut self) -> MeshUploader<B> {
        MeshUploader {
            data: Vec::new(),
            gpu: self.gpu.clone(),
            encoder: self.transfer.new_encoder(),
        }
    }
}

pub struct MeshUploader<B: Backend> {
    data: Vec<MeshData>,
    encoder: TransferEncoder<B>,
}
