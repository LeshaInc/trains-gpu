mod instance;
mod vertex;
// mod manager;

pub use self::instance::Instance;
pub use self::vertex::Vertex;

use gfx_hal::Backend;

use crate::buffer::Buffer;

pub struct Mesh<B: Backend> {
    pub vertex_buffer: Buffer<B, Vertex>,
    pub index_buffer: Buffer<B, u32>,
}

pub struct MeshData {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}
