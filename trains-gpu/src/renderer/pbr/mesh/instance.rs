use gfx_hal::format::Format;
use gfx_hal::pso::{AttributeDesc, Element, VertexBufferDesc, VertexInputRate};

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Instance {
    pub transform_col0: [f32; 3],
    pub transform_col1: [f32; 3],
    pub transform_col2: [f32; 3],
}

impl Instance {
    pub const VBUF_DESC: VertexBufferDesc = VertexBufferDesc {
        binding: 1,
        stride: 36,
        rate: VertexInputRate::Instance(1),
    };

    pub const ATTRS_DESC: &'static [AttributeDesc] = &[
        AttributeDesc {
            location: 0,
            binding: 1,
            element: Element {
                format: Format::Rgb32Sfloat,
                offset: 0,
            },
        },
        AttributeDesc {
            location: 1,
            binding: 1,
            element: Element {
                format: Format::Rgb32Sfloat,
                offset: 12,
            },
        },
        AttributeDesc {
            location: 2,
            binding: 1,
            element: Element {
                format: Format::Rgb32Sfloat,
                offset: 24,
            },
        },
    ];
}
