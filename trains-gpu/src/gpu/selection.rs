use gfx_hal::{
    adapter::{Adapter, DeviceType},
    queue::QueueFamily,
    window::Surface,
    Backend,
};

pub fn pick_families<B: Backend>(
    families: &[B::QueueFamily],
    surface: &B::Surface,
) -> Option<(usize, usize)> {
    log::trace!("Available queue families: ");
    for (i, family) in families.iter().enumerate() {
        log::trace!(
            " {}. type: {:?}, max-queues: {}",
            i,
            family.queue_type(),
            family.max_queues()
        );
    }

    let (graphics, _) = families.iter().enumerate().find(|(_id, family)| {
        surface.supports_queue_family(family) && family.queue_type().supports_graphics()
    })?;

    log::trace!("Picked graphics family #{}", graphics);

    let (transfer, _) = families
        .iter()
        .enumerate()
        .filter(|(_id, family)| family.queue_type().supports_transfer())
        .max_by_key(|(id, family)| {
            (!family.queue_type().supports_compute()) as u8
                + (!family.queue_type().supports_graphics()) as u8
                + (id != &graphics) as u8
        })?;

    log::trace!("Picked transfer family #{}", transfer);

    Some((graphics, graphics)) // TODO: switch back to (graphics, transfer) once separate transfer manager is implemented
}

pub fn pick_adapter<B: Backend>(mut adapters: Vec<Adapter<B>>) -> Option<Adapter<B>> {
    log::trace!("Available adapters:");
    let (idx, _) = adapters
        .iter()
        .enumerate()
        .filter(|(i, adapter)| {
            log::trace!(
                " {}. {}; type: {:?}; id: {}; vendor: {}",
                i,
                adapter.info.name,
                adapter.info.device_type,
                adapter.info.device,
                adapter.info.vendor
            );

            adapter
                .queue_families
                .iter()
                .any(|f| f.queue_type().supports_graphics())
        })
        .max_by_key(|(_, adapter)| match adapter.info.device_type {
            DeviceType::DiscreteGpu => 4,
            DeviceType::VirtualGpu => 3,
            DeviceType::IntegratedGpu => 2,
            DeviceType::Cpu => 1,
            DeviceType::Other => 0,
        })?;

    log::trace!("Picked adapter #{}", idx);

    Some(adapters.remove(idx))
}
