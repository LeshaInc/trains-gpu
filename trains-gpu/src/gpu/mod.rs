mod queues;
mod selection;

pub use self::queues::{QueueType, Queues};

use std::{
    mem::ManuallyDrop,
    sync::{Mutex, MutexGuard},
};

use anyhow::{anyhow, Result};
use gfx_hal::{adapter::PhysicalDevice, Backend, Instance};

use crate::{
    buffer::{Buffer, BufferUsage},
    image::{Image, ImageDesc},
    memory::{Allocator, MemoryUsage},
    util::Pod,
};

use self::selection::pick_adapter;

#[derive(Debug)]
pub struct Gpu<B: Backend> {
    instance: ManuallyDrop<B::Instance>,
    device: ManuallyDrop<B::Device>,
    physical_device: ManuallyDrop<B::PhysicalDevice>,
    allocator: ManuallyDrop<Mutex<Allocator<B>>>,
    queues: ManuallyDrop<Queues<B>>,
}

#[allow(dead_code)]
fn gpu_is_send_sync<B: Backend>() {
    fn f<T: Send + Sync>() {}
    f::<Gpu<B>>();
}

impl<B: Backend> Gpu<B> {
    pub fn new(instance: B::Instance, surface: &B::Surface) -> Result<Gpu<B>> {
        let adapter = pick_adapter(instance.enumerate_adapters())
            .ok_or_else(|| anyhow!("No GPUs detected"))?;
        let (device, physical_device, queues) = Queues::new(adapter, surface)?;
        let allocator = Allocator::new(&physical_device.memory_properties());

        let gpu = Gpu {
            instance: ManuallyDrop::new(instance),
            device: ManuallyDrop::new(device),
            physical_device: ManuallyDrop::new(physical_device),
            allocator: ManuallyDrop::new(Mutex::new(allocator)),
            queues: ManuallyDrop::new(queues),
        };

        Ok(gpu)
    }

    pub fn lock_allocator(&self) -> MutexGuard<'_, Allocator<B>> {
        self.allocator
            .lock()
            .expect("Unable to lock allocator mutex")
    }

    pub fn instance(&self) -> &B::Instance {
        &self.instance
    }

    pub fn physical_device(&self) -> &B::PhysicalDevice {
        &self.physical_device
    }

    pub fn device(&self) -> &B::Device {
        &self.device
    }

    pub fn queues(&self) -> &Queues<B> {
        &self.queues
    }

    pub fn create_buffer<T: Pod>(
        &self,
        len: usize,
        usage: BufferUsage,
        mem_usage: MemoryUsage,
    ) -> Result<Buffer<B, T>> {
        Buffer::new(&self, len, usage, mem_usage)
    }

    pub unsafe fn dispose_buffer<T: Pod>(&self, buffer: Buffer<B, T>) {
        buffer.dispose(&self)
    }

    pub fn create_image(&self, desc: &ImageDesc) -> Result<Image<B>> {
        Image::new(&self, &desc)
    }

    pub unsafe fn dispose_image(&self, image: Image<B>) {
        image.dispose(&self)
    }
}

impl<B: Backend> std::ops::Drop for Gpu<B> {
    fn drop(&mut self) {
        unsafe {
            ManuallyDrop::drop(&mut self.allocator);
            ManuallyDrop::drop(&mut self.queues);
            ManuallyDrop::drop(&mut self.physical_device);
            ManuallyDrop::drop(&mut self.device);
            ManuallyDrop::drop(&mut self.instance);
        }
    }
}
