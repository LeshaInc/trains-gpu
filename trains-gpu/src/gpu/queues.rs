use std::sync::{Mutex, MutexGuard};

use anyhow::{anyhow, Context, Result};

use gfx_hal::{
    adapter::{Adapter, PhysicalDevice},
    queue::QueueFamilyId,
    Backend, Features,
};

use super::selection::pick_families;

#[derive(Debug)]
pub struct Queues<B: Backend> {
    graphics: (Mutex<B::CommandQueue>, QueueFamilyId),
    transfer: (Mutex<B::CommandQueue>, QueueFamilyId),
    families: Vec<B::QueueFamily>,
}

#[derive(Clone, Copy, Debug)]
pub enum QueueType {
    Graphics,
    Transfer,
}

impl<B: Backend> Queues<B> {
    pub fn new(
        adapter: Adapter<B>,
        surface: &B::Surface,
    ) -> Result<(B::Device, B::PhysicalDevice, Queues<B>)> {
        let (graphics_family, transfer_family) =
            pick_families::<B>(&adapter.queue_families, surface)
                .ok_or_else(|| anyhow!("Can't pick queue families"))?;

        let features = Features::empty();
        let gpu = if graphics_family == transfer_family {
            let family = &adapter.queue_families[graphics_family];
            unsafe {
                adapter
                    .physical_device
                    .open(&[(family, &[1.0, 0.5])], features)
            }
        } else {
            let graphics = &adapter.queue_families[graphics_family];
            let transfer = &adapter.queue_families[transfer_family];
            unsafe {
                adapter
                    .physical_device
                    .open(&[(graphics, &[1.0]), (transfer, &[1.0])], features)
            }
        }
        .context("Can't open logical device")?;

        let device = gpu.device;
        let mut queue_groups = gpu.queue_groups;

        let graphics_group = queue_groups
            .iter_mut()
            .find(|q| q.family.0 == graphics_family)
            .ok_or_else(|| anyhow!("Can't open logical device"))?;
        let graphics_queue = graphics_group.queues.remove(0);

        let transfer_group = queue_groups
            .iter_mut()
            .find(|q| q.family.0 == transfer_family)
            .ok_or_else(|| anyhow!("Can't open logical device"))?;
        let transfer_queue = transfer_group.queues.remove(0);

        let queues = Queues {
            graphics: (Mutex::new(graphics_queue), QueueFamilyId(graphics_family)),
            transfer: (Mutex::new(transfer_queue), QueueFamilyId(transfer_family)),
            families: adapter.queue_families,
        };

        Ok((device, adapter.physical_device, queues))
    }

    pub fn is_unified(&self) -> bool {
        self.graphics.1 == self.transfer.1
    }

    pub fn graphics_family_id(&self) -> QueueFamilyId {
        self.graphics.1
    }

    pub fn lock_graphics(&self) -> MutexGuard<'_, B::CommandQueue> {
        self.graphics
            .0
            .lock()
            .expect("Unable to lock graphics queue")
    }

    pub fn transfer_family_id(&self) -> QueueFamilyId {
        self.transfer.1
    }

    pub fn lock_transfer(&self) -> MutexGuard<'_, B::CommandQueue> {
        self.transfer
            .0
            .lock()
            .expect("Unable to lock transfer queue")
    }

    pub fn family_id(&self, ty: QueueType) -> QueueFamilyId {
        match ty {
            QueueType::Transfer => self.transfer_family_id(),
            QueueType::Graphics => self.graphics_family_id(),
        }
    }

    pub fn lock(&self, ty: QueueType) -> MutexGuard<'_, B::CommandQueue> {
        match ty {
            QueueType::Transfer => self.lock_transfer(),
            QueueType::Graphics => self.lock_graphics(),
        }
    }
}
