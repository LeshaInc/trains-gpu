mod transfer;

pub use self::transfer::{
    CopyCommand, RawCopyCommand, RawUploadCommand, TransferEncoder, TransferManager, UploadCommand,
};

use gfx_hal::{
    command::Level,
    device::{Device, OomOrDeviceLost, OutOfMemory},
    pool::{CommandPool, CommandPoolCreateFlags},
    Backend,
};

use crate::{
    gpu::{Gpu, QueueType},
    util::{Frame, FrameData, FrameTracker, Relevant},
};

#[derive(Debug)]
struct PerFrame<B: Backend> {
    pool: B::CommandPool,
    fence: B::Fence,
    primary_buffers: Vec<Option<B::CommandBuffer>>,
    primary_index: usize,
    secondary_buffers: Vec<Option<B::CommandBuffer>>,
    secondary_index: usize,
}

#[derive(Debug)]
pub struct CommandCirque<B: Backend> {
    frames: FrameData<PerFrame<B>>,
    cur_frame: Frame,
    relevant: Relevant<Self>,
}

impl<B: Backend> CommandCirque<B> {
    pub fn new(
        gpu: &Gpu<B>,
        tracker: &FrameTracker,
        queue: QueueType,
    ) -> Result<Self, OutOfMemory> {
        let frames = FrameData::new(tracker, |_| {
            let family = gpu.queues().family_id(queue);
            let pool = unsafe {
                gpu.device()
                    .create_command_pool(family, CommandPoolCreateFlags::empty())
            }?;

            Ok(PerFrame {
                pool,
                fence: gpu.device().create_fence(true)?,
                primary_buffers: Vec::with_capacity(64),
                primary_index: 0,
                secondary_buffers: Vec::with_capacity(64),
                secondary_index: 0,
            })
        })?;

        Ok(CommandCirque {
            frames,
            cur_frame: tracker.current(),
            relevant: Relevant::new(),
        })
    }

    pub fn begin_frame(&mut self, gpu: &Gpu<B>, frame: Frame) -> Result<(), OomOrDeviceLost> {
        log::trace!("Begin frame {:?}", frame);
        self.cur_frame = frame;

        let mut frame = &mut self.frames[frame];
        frame.primary_index = 0;
        frame.secondary_index = 0;

        unsafe {
            let t = std::time::Instant::now();
            gpu.device().wait_for_fence(&frame.fence, !0)?;
            log::trace!("Wait {:?} for fence {:?}", t.elapsed(), &frame.fence);
            gpu.device().reset_fence(&frame.fence)?;
            frame.pool.reset(false);
        }

        Ok(())
    }

    pub fn current_fence(&self) -> &B::Fence {
        &self.frames[self.cur_frame].fence
    }

    pub fn get_primary_buffer(&mut self) -> B::CommandBuffer {
        let mut frame = &mut self.frames[self.cur_frame];
        if frame.primary_index >= frame.primary_buffers.len() {
            let buffer = unsafe { frame.pool.allocate_one(Level::Primary) };
            frame.primary_buffers.push(Some(buffer));
        }

        let buffer = &mut frame.primary_buffers[frame.primary_index];
        frame.primary_index += 1;
        std::mem::take(buffer).unwrap()
    }

    pub fn return_primary_buffer(&mut self, buffer: B::CommandBuffer) {
        let frame = &mut self.frames[self.cur_frame];
        for i in (0..frame.primary_index).rev() {
            if frame.primary_buffers[i].is_none() {
                frame.primary_buffers[i] = Some(buffer);
                break;
            }
        }
    }

    pub fn get_secondary_buffer(&mut self) -> B::CommandBuffer {
        let frame = &mut self.frames[self.cur_frame];
        if frame.secondary_index >= frame.secondary_buffers.len() {
            let buffer = unsafe { frame.pool.allocate_one(Level::Secondary) };
            frame.secondary_buffers.push(Some(buffer));
        }

        let buffer = &mut frame.secondary_buffers[frame.secondary_index];
        frame.secondary_index += 1;
        std::mem::take(buffer).unwrap()
    }

    pub fn return_secondary_buffer(&mut self, buffer: B::CommandBuffer) {
        let frame = &mut self.frames[self.cur_frame];
        for i in (0..frame.secondary_index).rev() {
            if frame.secondary_buffers[i].is_none() {
                frame.secondary_buffers[i] = Some(buffer);
                break;
            }
        }
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.relevant.dispose();
        for frame in self.frames.drain() {
            drop(frame.primary_buffers);
            drop(frame.secondary_buffers);
            gpu.device().destroy_command_pool(frame.pool);
            gpu.device().destroy_fence(frame.fence);
        }
    }
}
