//! FIXME: this module is broken

use std::time::Duration;

use anyhow::{anyhow, Result};

use gfx_hal::{
    buffer::{Access as BufferAccess, SubRange},
    command::{BufferCopy, CommandBuffer, CommandBufferFlags, Level},
    device::{Device, OomOrDeviceLost},
    memory::{Barrier, Dependencies},
    pool::{CommandPool, CommandPoolCreateFlags},
    pso::PipelineStage,
    query::{Query, ResultFlags, Type},
    queue::CommandQueue,
    Backend,
};

use crate::{
    buffer::{inspect_as_bytes, Buffer, StagingBuffer},
    cleaner::FrameCleaner,
    gpu::{Gpu, QueueType},
    util::{Frame, FrameData, FrameTracker, Pod, Relevant},
};

struct PerFrame<B: Backend> {
    submitted_buffers: Vec<B::CommandBuffer>,
    fence: B::Fence,
    query_pool: B::QueryPool,
    query_pool_size: u32,
    next_query_id: u32,
    init_buffer: B::CommandBuffer,
    query_target: Vec<u64>,
    first_time: bool,
    staging_buffers: Vec<StagingBuffer<B, u8>>,
}

fn create_query_pool<B: Backend>(
    device: &B::Device,
    pool: &mut B::CommandPool,
    size: u32,
) -> Result<(B::QueryPool, B::CommandBuffer)> {
    let query_pool = unsafe { device.create_query_pool(Type::Timestamp, size) }
        .map_err(|_| anyhow!("Can't create query pool"))?;
    let mut init_buffer;

    unsafe {
        init_buffer = pool.allocate_one(Level::Primary);
        init_buffer.begin(CommandBufferFlags::EMPTY, Default::default());
        init_buffer.reset_query_pool(&query_pool, 0..size);
        init_buffer.write_timestamp(
            PipelineStage::TRANSFER,
            Query {
                pool: &query_pool,
                id: 0,
            },
        );
        init_buffer.finish();
    }

    Ok((query_pool, init_buffer))
}

unsafe fn u64_as_u8(slice: &mut [u64]) -> &mut [u8] {
    std::slice::from_raw_parts_mut(slice.as_mut_ptr() as *mut u8, slice.len() * 8)
}

impl<B: Backend> PerFrame<B> {
    pub fn new(pool: &mut B::CommandPool, gpu: &Gpu<B>) -> Result<Self> {
        let fence = gpu.device().create_fence(true)?;
        let query_pool_size = 16;
        let (query_pool, init_buffer) =
            create_query_pool::<B>(gpu.device(), pool, query_pool_size)?;

        Ok(Self {
            submitted_buffers: Vec::new(),
            fence,
            query_pool,
            query_pool_size,
            next_query_id: 1,
            init_buffer,
            query_target: vec![0; query_pool_size as usize],
            first_time: true,
            staging_buffers: vec![],
        })
    }

    fn update_gpu_timing(&mut self, device: &B::Device) -> Result<(), OomOrDeviceLost> {
        if self.first_time {
            return Ok(());
        }

        let size = self.next_query_id;
        let target = &mut self.query_target[0..size as usize];

        let ready = unsafe {
            device.get_query_pool_results(
                &self.query_pool,
                0..size,
                u64_as_u8(target),
                8,
                // wait is unnecessary here, as we've already waited for the fence
                // keeping it just in case GPU needs more time to process query (unlikely)
                ResultFlags::BITS_64 | ResultFlags::WAIT,
            )
        }?;

        if !ready {
            // shouldn't happen
            log::warn!("Query isn't ready yet despite WAIT flag being set");
            return Ok(());
        }

        let start = target[0];
        let took = target[1..].iter().map(|end| end - start).max().unwrap_or(0);
        let took = Duration::from_nanos(took);
        log::trace!("Frame took {:?} on GPU", took);

        Ok(())
    }
}

pub struct TransferManager<B: Backend> {
    pool: B::CommandPool,
    available_buffers: Vec<B::CommandBuffer>,
    available_staging_buffers: Vec<StagingBuffer<B, u8>>,
    queued_buffers: Vec<B::CommandBuffer>,
    frames: FrameData<PerFrame<B>>,
    frame_tracker: FrameTracker,
    relevant: Relevant<Self>,
}

impl<B: Backend> TransferManager<B> {
    pub fn new(gpu: &Gpu<B>) -> Result<Self> {
        let family = gpu.queues().family_id(QueueType::Graphics);
        let flags = CommandPoolCreateFlags::RESET_INDIVIDUAL;
        let mut pool = unsafe { gpu.device().create_command_pool(family, flags) }?;

        let frame_tracker = FrameTracker::new(2);
        let frames = FrameData::new(&frame_tracker, |_| PerFrame::new(&mut pool, &gpu))?;

        Ok(Self {
            pool,
            available_buffers: Vec::new(),
            queued_buffers: Vec::new(),
            available_staging_buffers: Vec::new(),
            frames,
            frame_tracker,
            relevant: Relevant::new(),
        })
    }

    pub fn new_encoder(&mut self) -> TransferEncoder<B> {
        let mut command_buffer = if let Some(mut buffer) = self.available_buffers.pop() {
            unsafe { buffer.reset(false) };
            buffer
        } else {
            unsafe { self.pool.allocate_one(Level::Primary) }
        };

        unsafe {
            command_buffer.begin(CommandBufferFlags::ONE_TIME_SUBMIT, Default::default());
        }

        let mut staging_buffer = self
            .available_staging_buffers
            .pop()
            .unwrap_or_else(|| StagingBuffer::new());
        staging_buffer.clear();

        TransferEncoder {
            command_buffer,
            staging_buffer,
            frame: self.frame_tracker.current(), // FIXME: shouldn't be like this
            cleaner: FrameCleaner::new(&self.frame_tracker),
        }
    }

    pub fn begin_frame(&mut self, gpu: &Gpu<B>) -> Result<(), OomOrDeviceLost> {
        let frame_id = self.frame_tracker.current();
        let frame = &mut self.frames[frame_id];
        log::trace!("Begin transfer frame {:?}", frame_id);

        unsafe {
            let t = std::time::Instant::now();
            gpu.device().wait_for_fence(&frame.fence, !0)?;
            log::trace!("Wait {:?} for fence", t.elapsed());
            gpu.device().reset_fence(&frame.fence)?;
        }

        frame.update_gpu_timing(gpu.device())?;

        if frame.next_query_id >= frame.query_pool_size {
            frame.query_pool_size = frame.next_query_id + 1;
            let (mut query_pool, mut init_buffer) =
                create_query_pool::<B>(gpu.device(), &mut self.pool, frame.query_pool_size)
                    .unwrap(); // TODO

            frame.query_target = vec![0; frame.query_pool_size as usize];

            std::mem::swap(&mut query_pool, &mut frame.query_pool);
            std::mem::swap(&mut init_buffer, &mut frame.init_buffer);

            unsafe {
                init_buffer.reset(true);
                self.pool.free(Some(init_buffer));
                gpu.device().destroy_query_pool(query_pool);
            }
        }

        frame.next_query_id = 1;

        self.available_staging_buffers
            .append(&mut frame.staging_buffers);
        self.available_buffers.append(&mut frame.submitted_buffers);

        Ok(())
    }

    pub fn submit(&mut self, mut encoder: TransferEncoder<B>) {
        let frame = &mut self.frames[self.frame_tracker.current()];
        frame.staging_buffers.push(encoder.staging_buffer);
        unsafe {
            if frame.next_query_id < frame.query_pool_size {
                encoder.command_buffer.write_timestamp(
                    PipelineStage::TRANSFER,
                    Query {
                        pool: &frame.query_pool,
                        id: frame.next_query_id,
                    },
                );
            }
            frame.next_query_id += 1;
            encoder.command_buffer.finish();
        }
        self.queued_buffers.push(encoder.command_buffer);
    }

    pub fn flush(&mut self, gpu: &Gpu<B>) {
        if self.queued_buffers.is_empty() {
            return;
        }

        let frame_id = self.frame_tracker.current();
        let frame = &mut self.frames[frame_id];
        log::trace!(
            "Submit transfer frame {:?}; num_buffers: {}",
            frame_id,
            self.queued_buffers.len()
        );

        let mut transfer = gpu.queues().lock(QueueType::Graphics);
        unsafe {
            transfer.submit_without_semaphores(
                std::iter::once(&frame.init_buffer).chain(&self.queued_buffers),
                Some(&frame.fence),
            )
        };

        assert!(frame.submitted_buffers.is_empty());
        std::mem::swap(&mut frame.submitted_buffers, &mut self.queued_buffers);

        frame.first_time = false;
        self.frame_tracker.next();
    }

    pub fn wait_for_all(&mut self, gpu: &Gpu<B>) -> Result<(), OomOrDeviceLost> {
        log::trace!("Called TransferManager::wait_for_all");
        for frame in self.frames.iter() {
            if frame.first_time {
                continue;
            }

            unsafe {
                let t = std::time::Instant::now();
                gpu.device().wait_for_fence(&frame.fence, !0)?;
                log::trace!("Wait {:?} for fence", t.elapsed());
                gpu.device().reset_fence(&frame.fence)?;
            }
        }
        Ok(())
    }

    pub unsafe fn dispose(mut self, gpu: &Gpu<B>) {
        let device = gpu.device();

        for mut frame in self.frames.drain() {
            frame.init_buffer.reset(true);
            self.pool.free(Some(frame.init_buffer));
            device.destroy_query_pool(frame.query_pool);
            device.destroy_fence(frame.fence);
            for buffer in &mut frame.submitted_buffers {
                buffer.reset(true);
            }
            for staging_buffer in frame.staging_buffers {
                staging_buffer.dispose(gpu);
            }
            self.pool.free(frame.submitted_buffers);
        }

        for staging_buffer in self.available_staging_buffers {
            staging_buffer.dispose(gpu);
        }

        self.pool.free(self.available_buffers);
        self.pool.free(self.queued_buffers);

        device.destroy_command_pool(self.pool);

        self.relevant.dispose();
    }
}

pub struct TransferEncoder<B: Backend> {
    command_buffer: B::CommandBuffer,
    staging_buffer: StagingBuffer<B, u8>,
    frame: Frame,
    cleaner: FrameCleaner<B>,
}

#[derive(Clone, Debug)]
pub struct UploadCommand<'a, B: Backend, T: Pod> {
    pub buffer: &'a Buffer<B, T>,
    pub data: &'a [T],
    pub start_state: Option<(PipelineStage, BufferAccess)>,
    pub end_state: (PipelineStage, BufferAccess),
}

impl<'a, B: Backend, T: Pod> From<UploadCommand<'a, B, T>> for RawUploadCommand<'a, B> {
    fn from(typed: UploadCommand<'a, B, T>) -> Self {
        Self {
            buffer: typed.buffer.raw(),
            data: inspect_as_bytes(typed.data),
            start_state: typed.start_state,
            end_state: typed.end_state,
        }
    }
}

#[derive(Clone, Debug)]
pub struct RawUploadCommand<'a, B: Backend> {
    pub buffer: &'a B::Buffer,
    pub data: &'a [u8],
    pub start_state: Option<(PipelineStage, BufferAccess)>,
    pub end_state: (PipelineStage, BufferAccess),
}

#[derive(Clone, Debug)]
pub struct CopyCommand<'a, B: Backend, T: Pod> {
    pub src: &'a Buffer<B, T>,
    pub dst: &'a Buffer<B, T>,
    pub offset: u64,
    pub size: u64,
    pub dst_start_state: Option<(PipelineStage, BufferAccess)>,
    pub dst_end_state: (PipelineStage, BufferAccess),
}

#[derive(Clone, Debug)]
pub struct RawCopyCommand<'a, B: Backend> {
    pub src: &'a B::Buffer,
    pub dst: &'a B::Buffer,
    pub offset: u64,
    pub size: u64,
    pub dst_start_state: Option<(PipelineStage, BufferAccess)>,
    pub dst_end_state: (PipelineStage, BufferAccess),
}

impl<B: Backend> TransferEncoder<B> {
    pub unsafe fn upload_buffer<'a, T: Pod>(
        &mut self,
        gpu: &Gpu<B>,
        command: UploadCommand<'a, B, T>,
    ) -> Result<()> {
        self.upload_buffers(gpu, std::iter::once(command))
    }

    pub unsafe fn upload_buffers<'a, T, I>(&mut self, gpu: &Gpu<B>, commands: I) -> Result<()>
    where
        T: Pod,
        I: IntoIterator<Item = UploadCommand<'a, B, T>>,
        I::IntoIter: Clone,
    {
        self.upload_buffers_raw(gpu, commands.into_iter().map(Into::into))
    }

    pub unsafe fn upload_buffers_raw<'a, I>(&mut self, gpu: &Gpu<B>, commands: I) -> Result<()>
    where
        I: IntoIterator<Item = RawUploadCommand<'a, B>>,
        I::IntoIter: Clone,
    {
        let commands = commands.into_iter();
        let total_size = commands.clone().map(|cmd| cmd.data.len()).sum();
        self.staging_buffer.reserve(total_size);

        let mut offset = self.staging_buffer.byte_len();

        for cmd in commands.clone() {
            self.staging_buffer
                .extend_from_slice(gpu, &mut self.cleaner, cmd.data)?;
        }

        let staging_buffer = self.staging_buffer.raw().unwrap();
        let commands = commands.map(move |cmd| {
            let cmd = RawCopyCommand {
                src: staging_buffer,
                dst: cmd.buffer,
                offset,
                size: cmd.data.len() as u64,
                dst_start_state: cmd.start_state,
                dst_end_state: cmd.end_state,
            };
            offset += cmd.size;
            cmd
        });

        copy_buffers_raw::<B, _>(&mut self.command_buffer, commands);

        Ok(())
    }

    pub unsafe fn copy_buffers_raw<'a, I>(&mut self, commands: I)
    where
        I: IntoIterator<Item = RawCopyCommand<'a, B>>,
        I::IntoIter: Clone,
    {
        copy_buffers_raw(&mut self.command_buffer, commands);
    }
}

unsafe fn copy_buffers_raw<'a, B, I>(command_buffer: &mut B::CommandBuffer, commands: I)
where
    B: Backend,
    I: IntoIterator<Item = RawCopyCommand<'a, B>>,
    I::IntoIter: Clone,
{
    let commands = commands.into_iter();
    let mut dst_start_stage = PipelineStage::empty();
    let mut dst_end_stage = PipelineStage::empty();

    for cmd in commands.clone() {
        if let Some((stage, _)) = cmd.dst_start_state {
            dst_start_stage |= stage;
        }
        dst_end_stage |= cmd.dst_end_state.0;
    }

    if !dst_start_stage.is_empty() {
        let barriers = commands.clone().flat_map(|cmd| {
            Some(Barrier::Buffer {
                states: cmd.dst_start_state?.1..BufferAccess::TRANSFER_WRITE,
                target: cmd.dst,
                families: None,
                range: SubRange::WHOLE,
            })
        });

        command_buffer.pipeline_barrier(
            dst_start_stage..PipelineStage::TRANSFER,
            Dependencies::empty(),
            barriers,
        );
    }

    for cmd in commands.clone() {
        command_buffer.copy_buffer(
            cmd.src,
            cmd.dst,
            Some(BufferCopy {
                src: cmd.offset,
                dst: 0,
                size: cmd.size,
            }),
        );
    }

    let barriers = commands.map(|cmd| Barrier::Buffer {
        states: BufferAccess::TRANSFER_WRITE..cmd.dst_end_state.1,
        target: cmd.dst,
        families: None,
        range: SubRange::WHOLE,
    });

    command_buffer.pipeline_barrier(
        PipelineStage::TRANSFER..dst_end_stage,
        Dependencies::empty(),
        barriers,
    );
}

#[allow(dead_code)]
fn encoder_is_send_sync<B: Backend>() {
    fn f<T: Send + Sync>() {}
    f::<TransferEncoder<B>>();
}
