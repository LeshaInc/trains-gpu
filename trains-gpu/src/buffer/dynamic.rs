use std::ops::Range;

use anyhow::Result;

use gfx_hal::{
    buffer::SubRange,
    command::{BufferCopy, CommandBuffer},
    memory::{Barrier, Dependencies},
    pso::PipelineStage,
    Backend,
};

use crate::{cleaner::FrameCleaner, gpu::Gpu, memory::MemoryUsage, util::Pod};

use super::{Buffer, BufferAccess, BufferUsage, StagingBuffer};

pub struct DynamicBuffer<B: Backend, T: Pod> {
    staging: StagingBuffer<B, T>,
    host: Option<Buffer<B, T>>,
    usage: BufferUsage,
}

impl<B: Backend, T: Pod> DynamicBuffer<B, T> {
    pub fn new(usage: BufferUsage) -> DynamicBuffer<B, T> {
        DynamicBuffer {
            staging: StagingBuffer::new(),
            host: None,
            usage,
        }
    }

    pub fn len(&self) -> usize {
        self.staging.len()
    }

    pub fn reserve(&mut self, len: usize) {
        self.staging.reserve(len);
    }

    pub fn extend(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        items: impl Iterator<Item = T>,
    ) -> Result<()> {
        self.staging.extend(gpu, cleaner, items)
    }

    pub fn extend_from_slice(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        items: &[T],
    ) -> Result<()> {
        self.staging.extend_from_slice(gpu, cleaner, items)
    }

    pub fn clear(&mut self) {
        self.staging.clear();
    }

    pub fn raw(&self) -> &B::Buffer {
        self.host.as_ref().unwrap().raw()
    }

    pub unsafe fn upload(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        cbuf: &mut B::CommandBuffer,
        transition: Range<(PipelineStage, BufferAccess)>,
    ) -> Result<()> {
        let len = self.staging.len();
        if len == 0 {
            return Ok(());
        }

        let recreate = self.host.as_ref().filter(|v| v.len() > len).is_none();

        if recreate {
            let new = gpu.create_buffer(len, self.usage, MemoryUsage::GpuOnly)?;
            let old = std::mem::replace(&mut self.host, Some(new));
            if let Some(buffer) = old {
                cleaner.dispose_buffer(buffer);
            }
        }

        let staging = self.staging.raw().unwrap();
        let host = self.host.as_ref().unwrap().raw();

        cbuf.pipeline_barrier(
            transition.start.0..PipelineStage::TRANSFER,
            Dependencies::empty(),
            &[Barrier::Buffer {
                states: transition.start.1..BufferAccess::TRANSFER_WRITE,
                target: host,
                range: SubRange::WHOLE,
                families: None,
            }],
        );

        cbuf.copy_buffer(
            staging,
            host,
            Some(BufferCopy {
                src: 0,
                dst: 0,
                size: self.staging.byte_len(),
            }),
        );

        cbuf.pipeline_barrier(
            PipelineStage::TRANSFER..transition.end.0,
            Dependencies::empty(),
            &[Barrier::Buffer {
                states: BufferAccess::TRANSFER_WRITE..transition.end.1,
                target: host,
                range: SubRange::WHOLE,
                families: None,
            }],
        );

        Ok(())
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        if let Some(buf) = self.host {
            buf.dispose(gpu);
        }

        self.staging.dispose(gpu);
    }
}
