use gfx_hal::{
    pso::{AttributeDesc, GraphicsPipelineDesc, VertexBufferDesc},
    Backend,
};

use crate::util::Pod;

pub trait Vertex: Pod {
    fn vbuf_desc() -> VertexBufferDesc;

    fn attributes() -> Vec<AttributeDesc>;

    fn add_to_pipeline<B: Backend>(desc: &mut GraphicsPipelineDesc<'_, B>) {
        let binding = desc.vertex_buffers.len() as u32;
        let offset = desc.attributes.len() as u32;
        let attrs = Self::attributes().into_iter().map(|attr| AttributeDesc {
            location: attr.location + offset,
            binding,
            ..attr
        });

        desc.attributes.extend(attrs);
        desc.vertex_buffers.push(Self::vbuf_desc());
    }
}
