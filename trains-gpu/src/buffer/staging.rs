use anyhow::Result;

use gfx_hal::Backend;

use crate::{
    cleaner::FrameCleaner,
    gpu::Gpu,
    memory::{Memory, MemoryUsage},
    util::Pod,
};

use super::{inspect_as_bytes, Buffer, BufferUsage};

pub struct StagingBuffer<B: Backend, T: Pod> {
    buffer: Option<Buffer<B, T>>,
    len: usize,
    capacity: usize,
    new_capacity: usize,
}

impl<B: Backend, T: Pod> StagingBuffer<B, T> {
    pub fn new() -> StagingBuffer<B, T> {
        StagingBuffer {
            buffer: None,
            len: 0,
            capacity: 0,
            new_capacity: 0,
        }
    }

    pub fn raw(&self) -> Option<&B::Buffer> {
        self.buffer.as_ref().map(|b| b.raw())
    }

    pub fn memory(&self) -> Option<&Memory<B>> {
        self.buffer.as_ref().map(|b| b.memory())
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn byte_len(&self) -> u64 {
        let stride = std::mem::size_of::<T>() as u64;
        (self.len as u64) * stride
    }

    pub fn reserve(&mut self, len: usize) {
        if self.len + len <= self.new_capacity {
            return;
        }

        self.new_capacity = (self.len + len).next_power_of_two();
    }

    fn resize(&mut self, gpu: &Gpu<B>, cleaner: &mut FrameCleaner<B>) -> Result<()> {
        if self.capacity == self.new_capacity {
            return Ok(());
        }

        let mut new_buffer = gpu.create_buffer(
            self.new_capacity,
            BufferUsage::TRANSFER_SRC,
            MemoryUsage::CpuToGpu,
        )?;

        self.capacity = self.new_capacity;

        let mut old_buffer = match self.buffer.take() {
            Some(v) => v,
            None => {
                self.buffer = Some(new_buffer);
                return Ok(());
            }
        };

        let device = gpu.device();
        let len = old_buffer.byte_len() as usize;

        unsafe {
            // copy data from old buffer to new one
            let mut allocator = gpu.lock_allocator();
            let mut src = allocator.map::<u8>(device, &mut old_buffer.memory)?;
            let mut dst = allocator.map(device, &mut new_buffer.memory)?;

            src.invalidate(device, 0..len)?;
            let src_slice = &src.as_uninit_slice()[0..len];
            let dst_slice = &mut dst.as_uninit_slice()[0..len];
            dst_slice.copy_from_slice(src_slice);
            dst.flush(device, 0..len)?;

            allocator.unmap(device, src);
            allocator.unmap(device, dst);
            drop(allocator);
        }

        cleaner.dispose_buffer(old_buffer);
        self.buffer = Some(new_buffer);

        Ok(())
    }

    pub fn extend(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        items: impl Iterator<Item = T>,
    ) -> Result<()> {
        for item in items {
            // FIXME: looks very inefficient
            self.extend_from_slice(gpu, cleaner, &[item])?;
        }

        Ok(())
    }

    pub fn extend_from_slice(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        items: &[T],
    ) -> Result<()> {
        let len = items.len();
        self.reserve(len);
        self.resize(gpu, cleaner)?;

        let buffer = match &mut self.buffer {
            Some(v) => v,
            None => {
                // resize() didn't do anything
                assert_eq!(len, 0);
                return Ok(());
            }
        };

        let stride = std::mem::size_of::<T>();
        let start = self.len * stride;
        let additional_len = len * stride;
        let end = start + additional_len;

        let device = gpu.device();

        unsafe {
            let mut allocator = gpu.lock_allocator();
            let mut data = allocator.map(device, &mut buffer.memory)?;
            data.copy_from_slice(start..end, inspect_as_bytes(items));
            data.flush(device, start..end)?;
            allocator.unmap(device, data);
        }

        self.len += len;

        Ok(())
    }

    pub fn clear(&mut self) {
        self.len = 0;
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        if let Some(buf) = self.buffer {
            buf.dispose(gpu);
        }
    }
}
