mod dynamic;
mod staging;
mod vertex;

pub use gfx_hal::buffer::{Access as BufferAccess, Usage as BufferUsage};

pub use self::{dynamic::DynamicBuffer, staging::StagingBuffer, vertex::Vertex};

use std::{marker::PhantomData, mem::size_of};

use anyhow::{Context, Result};
use gfx_hal::{device::Device, Backend};

use crate::{
    gpu::Gpu,
    memory::{Memory, MemorySpec, MemoryUsage},
    util::{Pod, Relevant},
};

#[derive(Debug)]
pub struct Buffer<B: Backend, T: Pod> {
    inner: B::Buffer,
    len: usize,
    memory: Memory<B>,
    relevant: Relevant<Self>,
    _marker: PhantomData<T>,
}

impl<B: Backend, T: Pod> Buffer<B, T> {
    pub fn new(
        gpu: &Gpu<B>,
        len: usize,
        usage: BufferUsage,
        mem_usage: MemoryUsage,
    ) -> Result<Self> {
        let device = gpu.device();

        let len_bytes = len * size_of::<T>();
        let mut buffer = unsafe {
            device
                .create_buffer(len_bytes as u64, usage)
                .context("Failed to create buffer object")?
        };

        let requirements = unsafe { device.get_buffer_requirements(&buffer) };
        let spec = MemorySpec::new(requirements).with_usage(mem_usage);
        let memory = gpu
            .lock_allocator()
            .allocate(device, spec)
            .context(format!(
                "Failed to allocate buffer memory ({} bytes)",
                spec.requirements.size
            ))?;

        unsafe {
            device
                .bind_buffer_memory(memory.inner(), memory.range().start, &mut buffer)
                .context("Failed to bind buffer memory")?;
        };

        Ok(Buffer {
            inner: buffer,
            len,
            memory,
            relevant: Relevant::new(),
            _marker: PhantomData,
        })
    }

    pub fn raw(&self) -> &B::Buffer {
        &self.inner
    }

    pub fn memory(&self) -> &Memory<B> {
        &self.memory
    }

    pub fn transmute<U: Pod>(self) -> Buffer<B, U> {
        let new_len = self.len * std::mem::size_of::<T>() / std::mem::size_of::<U>();
        self.relevant.dispose();
        Buffer {
            inner: self.inner,
            len: new_len,
            memory: self.memory,
            relevant: Relevant::new(),
            _marker: PhantomData,
        }
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.relevant.dispose();
        gpu.device().destroy_buffer(self.inner);
        gpu.lock_allocator().free(gpu.device(), self.memory);
    }

    pub fn byte_len(&self) -> u64 {
        let stride = std::mem::size_of::<T>() as u64;
        (self.len as u64) * stride
    }

    pub fn len(&self) -> usize {
        self.len
    }
}

pub fn inspect_as_bytes<T: Pod>(slice: &[T]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(
            slice.as_ptr() as *const u8,
            slice.len() * std::mem::size_of::<T>(),
        )
    }
}
