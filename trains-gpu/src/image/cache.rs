use std::{collections::HashMap, hash::Hash, ops::Range};

use gfx_hal::{
    command::{BufferImageCopy, CommandBuffer, ImageCopy},
    format::{Aspects, Format},
    image::{Extent, Offset, SubresourceLayers, SubresourceRange},
    memory::{Barrier, Dependencies},
    pso::PipelineStage,
    Backend,
};

use anyhow::Result;
use guillotiere::{AllocId, AtlasAllocator};
use vek::{Aabr, Extent2, Vec2};

use crate::{buffer::StagingBuffer, cleaner::FrameCleaner, gpu::Gpu};

use super::{Image, ImageAccess, ImageDesc, ImageLayout, ImageUsage};

struct Entry<V> {
    id: AllocId,
    size: (u32, u32),
    offset: (u32, u32),
    data: V,
}

struct NewEntry<K, V> {
    key: K,
    value: V,
    offset: u64,
    size: (u32, u32),
}

pub struct TextureCache<B: Backend, K: Hash + Eq, V = ()> {
    entries: HashMap<K, Entry<V>>,
    new_entries: Vec<NewEntry<K, V>>,
    allocator: AtlasAllocator,
    image: Image<B>,
    staging_buffer: StagingBuffer<B, u8>,
    clear_staging: bool,
    format: Format,
    usage: ImageUsage,
}

impl<B: Backend, K: Hash + Eq, V> TextureCache<B, K, V> {
    pub fn new(gpu: &Gpu<B>, size: (u32, u32), format: Format, usage: ImageUsage) -> Result<Self> {
        let desc = ImageDesc::new_2d(size.0, size.1)
            .with_format(format)
            .with_usage(usage);
        let image = gpu.create_image(&desc)?;

        Ok(TextureCache {
            entries: HashMap::new(),
            new_entries: Vec::new(),
            allocator: AtlasAllocator::new(size2d(size)),
            image,
            staging_buffer: StagingBuffer::new(),
            clear_staging: false,
            format,
            usage,
        })
    }

    pub fn size(&self) -> Extent2<u32> {
        let size = self.allocator.size();
        Extent2::new(size.width as u32, size.height as u32)
    }

    pub fn raw(&self) -> &B::Image {
        self.image.raw()
    }

    pub fn view_raw(&self) -> &B::ImageView {
        self.image.view_raw()
    }

    pub fn add(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        key: K,
        value: V,
        size: (u32, u32),
        data: &[u8],
    ) -> Result<()> {
        if self.clear_staging {
            // flag has been set in .flush() method to clear staging buffer in the next frame
            self.clear_staging = false;
            self.staging_buffer.clear();
        }

        self.new_entries.push(NewEntry {
            key,
            value,
            offset: self.staging_buffer.byte_len(),
            size,
        });

        self.staging_buffer.extend_from_slice(gpu, cleaner, data)?;

        // buffer copy command accepts offsets multiple of 4, add padding bytes
        let offset = self.staging_buffer.byte_len();
        let align = 4 - (offset & 3);
        let zeros = [0; 4];
        self.staging_buffer
            .extend_from_slice(gpu, cleaner, &zeros[0..align as usize])
    }

    pub fn get_rect(&self, key: &K) -> Option<Aabr<u32>> {
        let entry = self.entries.get(key)?;
        let min = Vec2::new(entry.offset.0, entry.offset.1);
        Some(Aabr {
            min,
            max: min + Vec2::new(entry.size.0, entry.size.1),
        })
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        self.entries.get(key).map(|entry| &entry.data)
    }

    pub fn get_mut(&mut self, key: &K) -> Option<&mut V> {
        self.entries.get_mut(key).map(|entry| &mut entry.data)
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (&K, &mut V)> + '_ {
        self.entries.iter_mut().map(|(k, v)| (k, &mut v.data))
    }

    pub fn retain(&mut self, mut f: impl FnMut(&K, &mut V) -> bool) {
        let allocator = &mut self.allocator;
        self.entries.retain(|key, entry| {
            if f(key, &mut entry.data) {
                true
            } else {
                allocator.deallocate(entry.id);
                false
            }
        })
    }

    pub fn remove(&mut self, key: &K) {
        if let Some(old_entry) = self.entries.remove(key) {
            self.allocator.deallocate(old_entry.id);
        }
    }

    pub fn contains(&self, key: &K) -> bool {
        self.entries.contains_key(key)
    }

    pub fn flush(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        buf: &mut B::CommandBuffer,
        transition: Range<(PipelineStage, ImageAccess, ImageLayout)>,
    ) -> Result<bool> {
        let mut entries_remaining = self.new_entries.len();
        let mut copy_regions = Vec::with_capacity(entries_remaining);
        let mut entries_drain = self.new_entries.drain(..);

        let old_size = self.allocator.size();
        let mut new_size = None;

        let subrs_layers = SubresourceLayers {
            aspects: Aspects::COLOR,
            level: 0,
            layers: 0..1,
        };
        let subrs_range = SubresourceRange {
            aspects: Aspects::COLOR,
            levels: 0..1,
            layers: 0..1,
        };

        let mut failed_entry = None;

        while entries_remaining > 0 {
            let iter = failed_entry.take().into_iter().chain(&mut entries_drain);
            for entry in iter {
                let alloc = match self.allocator.allocate(size2d(entry.size)) {
                    Some(v) => v,
                    None => {
                        failed_entry = Some(entry);
                        break;
                    }
                };

                let offset = alloc.rectangle.min.to_u32().into();

                self.entries.insert(
                    entry.key,
                    Entry {
                        id: alloc.id,
                        size: entry.size,
                        offset,
                        data: entry.value,
                    },
                );

                copy_regions.push(BufferImageCopy {
                    buffer_offset: entry.offset,
                    buffer_width: entry.size.0,
                    buffer_height: entry.size.1,
                    image_layers: subrs_layers.clone(),
                    image_offset: Offset {
                        x: offset.0 as i32,
                        y: offset.1 as i32,
                        z: 0,
                    },
                    image_extent: Extent {
                        width: entry.size.0,
                        height: entry.size.1,
                        depth: 1,
                    },
                });

                entries_remaining -= 1;
            }

            if entries_remaining > 0 {
                let s = self.allocator.size() * 2;
                new_size = Some(s);
                log::info!("new size: {:?}", s);
                self.allocator.grow(s);
            }
        }

        if copy_regions.is_empty() {
            return Ok(false);
        }

        let mut has_resized = false;

        if let Some(size) = new_size {
            let desc = ImageDesc::new_2d(size.width as _, size.height as _)
                .with_format(self.format)
                .with_usage(self.usage);
            let new = gpu.create_image(&desc)?;
            let old = std::mem::replace(&mut self.image, new);

            unsafe {
                buf.pipeline_barrier(
                    transition.start.0..PipelineStage::TRANSFER,
                    Dependencies::empty(),
                    &[
                        Barrier::Image {
                            states: (transition.start.1, transition.start.2)
                                ..(ImageAccess::TRANSFER_READ, ImageLayout::TransferSrcOptimal),
                            target: old.raw(),
                            range: subrs_range.clone(),
                            families: None,
                        },
                        Barrier::Image {
                            states: (ImageAccess::empty(), ImageLayout::Undefined)
                                ..(ImageAccess::TRANSFER_WRITE, ImageLayout::TransferDstOptimal),
                            target: self.image.raw(),
                            range: subrs_range.clone(),
                            families: None,
                        },
                    ],
                );

                buf.copy_image(
                    old.raw(),
                    ImageLayout::TransferSrcOptimal,
                    self.image.raw(),
                    ImageLayout::TransferDstOptimal,
                    &[ImageCopy {
                        src_subresource: subrs_layers.clone(),
                        src_offset: Offset { x: 0, y: 0, z: 0 },
                        dst_subresource: subrs_layers,
                        dst_offset: Offset { x: 0, y: 0, z: 0 },
                        extent: Extent {
                            width: old_size.width as _,
                            height: old_size.height as _,
                            depth: 1,
                        },
                    }],
                );
            }

            cleaner.dispose_image(old);
            has_resized = true;
        } else {
            unsafe {
                buf.pipeline_barrier(
                    transition.start.0..PipelineStage::TRANSFER,
                    Dependencies::empty(),
                    &[Barrier::Image {
                        states: (transition.start.1, transition.start.2)
                            ..(ImageAccess::TRANSFER_WRITE, ImageLayout::TransferDstOptimal),
                        target: self.image.raw(),
                        range: subrs_range.clone(),
                        families: None,
                    }],
                );
            }
        }

        unsafe {
            buf.copy_buffer_to_image(
                self.staging_buffer.raw().unwrap(),
                self.image.raw(),
                ImageLayout::TransferDstOptimal,
                copy_regions,
            );

            buf.pipeline_barrier(
                PipelineStage::TRANSFER..transition.end.0,
                Dependencies::empty(),
                &[Barrier::Image {
                    states: (ImageAccess::TRANSFER_WRITE, ImageLayout::TransferDstOptimal)
                        ..(transition.end.1, transition.end.2),
                    target: self.image.raw(),
                    range: subrs_range,
                    families: None,
                }],
            );
        }

        // clear staging buffer in the next frame
        self.clear_staging = true;

        Ok(has_resized)
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.image.dispose(gpu);
        self.staging_buffer.dispose(gpu);
    }
}

fn size2d(s: (u32, u32)) -> guillotiere::Size {
    [s.0 as i32, s.1 as i32].into()
}
