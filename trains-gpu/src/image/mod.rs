mod cache;

pub use gfx_hal::image::{
    Access as ImageAccess, Kind as ImageKind, Layout as ImageLayout, Tiling as ImageTiling,
    Usage as ImageUsage,
};

pub use self::cache::TextureCache;

use gfx_hal::{
    device::Device,
    format::{Aspects, Format, Swizzle},
    image::{SubresourceRange, ViewCapabilities, ViewKind},
    Backend,
};

use anyhow::{Context, Result};

use crate::{
    gpu::Gpu,
    memory::{Memory, MemorySpec, MemoryUsage},
    util::Relevant,
};

#[derive(Clone, Copy, Debug)]
pub struct ImageDesc {
    kind: ImageKind,
    levels: u8,
    format: Format,
    tiling: ImageTiling,
    usage: ImageUsage,
    view_caps: ViewCapabilities,
    memory_usage: MemoryUsage,
}

impl ImageDesc {
    pub fn new_2d(width: u32, height: u32) -> ImageDesc {
        ImageDesc {
            kind: ImageKind::D2(width, height, 1, 1),
            levels: 1,
            format: Format::Rgba8Unorm,
            tiling: ImageTiling::Optimal,
            usage: ImageUsage::empty(),
            view_caps: ViewCapabilities::empty(),
            memory_usage: MemoryUsage::GpuOnly,
        }
    }

    pub fn with_format(mut self, format: Format) -> ImageDesc {
        self.format = format;
        self
    }

    pub fn with_usage(mut self, usage: ImageUsage) -> ImageDesc {
        self.usage = usage;
        self
    }
}

#[derive(Debug)]
pub struct Image<B: Backend> {
    inner: B::Image,
    view: B::ImageView,
    memory: Memory<B>,
    relevant: Relevant<Self>,
}

impl<B: Backend> Image<B> {
    pub fn new(gpu: &Gpu<B>, desc: &ImageDesc) -> Result<Image<B>> {
        let device = gpu.device();
        let mut image = unsafe {
            device
                .create_image(
                    desc.kind,
                    desc.levels,
                    desc.format,
                    desc.tiling,
                    desc.usage,
                    desc.view_caps,
                )
                .context("Failed to create image object")?
        };

        let requirements = unsafe { device.get_image_requirements(&image) };
        let spec = MemorySpec::new(requirements).with_usage(desc.memory_usage);
        let memory = gpu
            .lock_allocator()
            .allocate(device, spec)
            .context(format!(
                "Failed to allocate image memory ({} bytes)",
                spec.requirements.size
            ))?;

        unsafe {
            device
                .bind_image_memory(memory.inner(), memory.range().start, &mut image)
                .context("Failed to bind image memory")?;
        };

        let view = unsafe {
            device
                .create_image_view(
                    &image,
                    ViewKind::D2,
                    desc.format,
                    Swizzle::NO,
                    SubresourceRange {
                        aspects: Aspects::COLOR,
                        levels: 0..1,
                        layers: 0..1,
                    },
                )
                .context("Can't create image view")?
        };

        Ok(Image {
            inner: image,
            view,
            memory,
            relevant: Relevant::new(),
        })
    }

    pub fn raw(&self) -> &B::Image {
        &self.inner
    }

    pub fn view_raw(&self) -> &B::ImageView {
        &self.view
    }

    pub fn memory(&self) -> &Memory<B> {
        &self.memory
    }

    pub unsafe fn dispose(self, gpu: &Gpu<B>) {
        self.relevant.dispose();
        gpu.device().destroy_image_view(self.view);
        gpu.device().destroy_image(self.inner);
        gpu.lock_allocator().free(gpu.device(), self.memory);
    }
}
