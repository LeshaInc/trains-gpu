use gfx_hal::{device::Device, Backend};

use crate::{
    buffer::Buffer,
    gpu::Gpu,
    image::Image,
    util::{FrameData, FrameTracker, Pod},
};

struct PerFrame<B: Backend> {
    images: Vec<Image<B>>,
    buffers: Vec<Buffer<B, u8>>,
    framebuffers: Vec<B::Framebuffer>,
}

impl<B: Backend> PerFrame<B> {
    fn new() -> Self {
        PerFrame {
            images: Vec::new(),
            buffers: Vec::new(),
            framebuffers: Vec::new(),
        }
    }
}

pub struct FrameCleaner<B: Backend> {
    tracker: FrameTracker,
    data: FrameData<PerFrame<B>>,
}

impl<B: Backend> FrameCleaner<B> {
    pub fn new(tracker: &FrameTracker) -> Self {
        FrameCleaner {
            tracker: tracker.clone(),
            data: FrameData::new_infallible(tracker, |_| PerFrame::new()),
        }
    }

    pub fn dispose_image(&mut self, image: Image<B>) {
        let frame = self.tracker.current();
        self.data[frame].images.push(image)
    }

    pub fn dispose_buffer<T: Pod>(&mut self, buffer: Buffer<B, T>) {
        let frame = self.tracker.current();
        self.data[frame].buffers.push(buffer.transmute())
    }

    pub fn dispose_framebuffer(&mut self, framebuffer: B::Framebuffer) {
        let frame = self.tracker.current();
        self.data[frame].framebuffers.push(framebuffer)
    }

    pub unsafe fn cleanup(&mut self, gpu: &Gpu<B>) {
        let frame = &mut self.data[self.tracker.current()];
        frame.images.drain(..).for_each(|v| v.dispose(gpu));
        frame.buffers.drain(..).for_each(|v| v.dispose(gpu));
        for fb in frame.framebuffers.drain(..) {
            gpu.device().destroy_framebuffer(fb)
        }
    }

    pub fn next_frame(&mut self) {
        self.tracker.next();
    }
}
