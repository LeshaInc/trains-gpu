use std::sync::Arc;

use anyhow::{anyhow, Result};

use gfx_backend_vulkan::{Backend, Instance};
use gfx_hal::Instance as _;

use winit::{
    dpi::PhysicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use rand::prelude::*;
use rusttype::Font;
use vek::{Aabr, Rgba, Vec2};

use trains_gpu::{
    BlurCommand, BorderRadii, Command, DrawCommand, DrawList, FpsCounter, Rect, Renderer, Text,
};

fn main() -> Result<()> {
    env_logger::init();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(PhysicalSize::new(800, 600))
        .with_min_inner_size(PhysicalSize::new(800, 600))
        .with_max_inner_size(PhysicalSize::new(800, 600))
        .build(&event_loop)?;

    let window = Arc::new(window);

    let instance = Instance::create("test", 1).map_err(|_| anyhow!("Unsupported backend"))?;

    let mut renderer = Renderer::<Backend>::new(instance, window.clone())?;

    renderer.init()?;

    let t = std::time::Instant::now();

    let mut rng = rand::thread_rng();
    let mut commands = (0..300)
        .map(|_| {
            let origin = Vec2::new(rng.gen_range(-200.0, 1280.0), rng.gen_range(-200.0, 800.0));
            let size = Vec2::new(rng.gen_range(20.0, 200.0), rng.gen_range(20.0, 200.0));
            Command::Draw(DrawCommand::Rect(Rect {
                origin,
                size,
                color: Rgba::new(0.95, 0.98, 1.0, 1.0),
                shadow_color: Rgba::new(0.0, 0.0, 0.0, 0.7),
                shadow_offset: Vec2::new(0.0 * 4.0, 1.0 * 4.0),
                shadow_blur_radius: rng.gen_range(5.0, 50.0),
                border_radii: BorderRadii::new(8.0),
                border_color: Rgba::new(0.14, 0.55, 0.74, 1.0),
                border_thickness: 2.0,
            }))
        })
        .collect::<Vec<_>>();

    let font_data: &[u8] = include_bytes!("../../resources/OpenSans-Regular.ttf");
    let font: Font<'static> =
        Font::try_from_bytes(font_data).ok_or_else(|| anyhow!("Invalid font"))?;

    let mut y = 100.0;
    for i in 1..7 {
        let s = i as f32 * 8.0;
        commands.push(Command::Draw(DrawCommand::Text(Text {
            origin: Vec2::new(20.0, y),
            font_size: Vec2::new(s, s),
            color: Rgba::new(0.0, 0.0, 0.0, 1.0),
            font_id: 0,
            text: "The quick brown fox jumps over the lazy dog.".into(),
        })));
        y += s;
    }
    for i in 1..7 {
        let s = i as f32 * 8.0;
        commands.push(Command::Draw(DrawCommand::Text(Text {
            origin: Vec2::new(20.0, y),
            font_size: Vec2::new(s, s),
            color: Rgba::new(0.0, 0.0, 0.0, 1.0),
            font_id: 0,
            text: "Съешь ещё этих мягких французских булок, да выпей же чаю.".into(),
        })));
        y += s;
    }

    commands.push(Command::Draw(DrawCommand::Rect(Rect {
        origin: Vec2::new(20.0, 20.0),
        size: Vec2::new(200.0, 200.0),
        color: Rgba::new(1.0, 0.0, 0.0, 1.0),
        shadow_color: Rgba::new(0.0, 0.0, 0.0, 0.0),
        shadow_offset: Vec2::new(0.0 * 4.0, 1.0 * 4.0),
        shadow_blur_radius: rng.gen_range(5.0, 50.0),
        border_radii: BorderRadii::new(0.0),
        border_color: Rgba::new(0.14, 0.55, 0.74, 1.0),
        border_thickness: 0.0,
    })));

    commands.push(Command::Draw(DrawCommand::Rect(Rect {
        origin: Vec2::new(220.0, 20.0),
        size: Vec2::new(200.0, 200.0),
        color: Rgba::new(0.0, 1.0, 0.0, 1.0),
        shadow_color: Rgba::new(0.0, 0.0, 0.0, 0.0),
        shadow_offset: Vec2::new(0.0 * 4.0, 1.0 * 4.0),
        shadow_blur_radius: rng.gen_range(5.0, 50.0),
        border_radii: BorderRadii::new(0.0),
        border_color: Rgba::new(0.14, 0.55, 0.74, 1.0),
        border_thickness: 0.0,
    })));

    commands.push(Command::Blur(BlurCommand {
        region: Aabr {
            min: Vec2::new(20, 20),
            max: Vec2::new(420, 220),
        },
        radius: 15.0,
    }));

    commands.push(Command::Draw(DrawCommand::Text(Text {
        origin: Vec2::new(60.0, 50.0),
        font_size: Vec2::new(50.0, 50.0),
        color: Rgba::new(0.0, 0.0, 0.0, 1.0),
        font_id: 0,
        text: "".into(),
    })));

    let mut list = DrawList {
        commands,
        fonts: vec![font],
    };

    let mut fps_counter = FpsCounter::new(120);

    event_loop.run(move |event: Event<()>, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => *control_flow = ControlFlow::Exit,
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(_),
                ..
            } => {
                renderer.resize().unwrap();
            }
            Event::RedrawRequested(_) => {
                fps_counter.begin_frame();

                let (s, c) = (t.elapsed().as_secs_f32() * 3.0).sin_cos();
                for cmd in &mut list.commands {
                    match cmd {
                        Command::Draw(DrawCommand::Rect(r)) => {
                            r.shadow_offset = Vec2::new(c * 4.0, s * 4.0)
                        }
                        _ => (),
                    }
                }

                if let Some(Command::Draw(DrawCommand::Text(t))) = list.commands.last_mut() {
                    t.text = format!(
                        "{:5.2} FPS ({:.3?} per frame)",
                        fps_counter.avg_fps(),
                        fps_counter.avg_frametime()
                    );
                }

                if let Err(e) = renderer.render(&list) {
                    log::error!("{:?}", e);
                    *control_flow = ControlFlow::Exit;
                }

                fps_counter.end_frame();
            }
            _ => (),
        }
    });
}
