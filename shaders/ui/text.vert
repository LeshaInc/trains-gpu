#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;

layout(location = 1) in vec4 inTransform01;
layout(location = 2) in vec2 inTransform2;
layout(location = 3) in vec4 inColor;
layout(location = 4) in vec4 inUVRect;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec2 outUV;

void main() {
    outUV = inPos * inUVRect.zw + inUVRect.xy;
    outColor = inColor;

    mat3 transform = mat3(mat3x2(inTransform01.xy, inTransform01.zw, inTransform2));
    gl_Position = vec4(transform * vec3(inPos, 1.0), 1.0);
}