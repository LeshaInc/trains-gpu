#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inSize;
layout(location = 2) in vec4 inColor;
layout(location = 3) in vec4 inShadowColor;
layout(location = 4) in vec4 inBorderColor;
layout(location = 5) in vec4 inBorderRadii;
layout(location = 6) in vec2 inShadowOffset;
layout(location = 7) in float inBlurRadius;
layout(location = 8) in float inBorderThickness;

layout(location = 0) out vec4 outColor;

float roundedRect(vec2 p, vec2 b, vec4 r) {
    r.xy = (p.x>0.0) ? r.xy : r.zw;
    r.x = (p.y>0.0) ? r.x : r.y;
    vec2 q = abs(p) - b + r.x;
    return min(max(q.x,q.y), 0.0) + length(max(q, 0.0)) - r.x;
}

void main() {
    vec2 halfSize = inSize / 2;
    vec2 pos = min(vec2(inBlurRadius), inShadowOffset) + (inPos - vec2(0.5)) * (inSize + vec2(2 * inBlurRadius));
    
    float dist = roundedRect(pos, halfSize, inBorderRadii);
    float distanceChange = fwidth(dist) * 0.5;
    float borderDist = dist + inBorderThickness;
    float mask = smoothstep(distanceChange, -distanceChange, dist);
    float borderMask = smoothstep(distanceChange, -distanceChange, borderDist);

    vec4 color = mix(inBorderColor, inColor, borderMask);

    float shadowDist = roundedRect(pos - inShadowOffset, halfSize - inBlurRadius, inBorderRadii);
    float shadow = 1 - smoothstep(0, 2 * inBlurRadius, shadowDist);

    outColor = mix(vec4(inShadowColor.rgb, inShadowColor.a * shadow), color, mask);
}