#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec2 outUV;

layout(push_constant) uniform PushConstants {
  vec4 rect;
  vec2 resolution;
  vec2 direction;
} pc;

const vec2 POINTS[6] = {vec2(0, 0), vec2(1, 0), vec2(0, 1), vec2(0, 1), vec2(1, 0), vec2(1, 1)};

void main() {
  vec2 uv = POINTS[gl_VertexIndex] * pc.rect.zw + pc.rect.xy;
  outUV = uv;
  gl_Position = vec4(uv * 2 - 1, 0, 1);
}