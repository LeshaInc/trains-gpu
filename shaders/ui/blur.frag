#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inUV;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler2D uTexture;

layout(push_constant) uniform PushConstants {
  vec4 rect;
  vec2 resolution;
  vec2 direction;
} pc;

const float KERNEL[15] = {0.009033, 0.018476, 0.033851, 0.055555, 0.08167, 0.107545, 0.126854, 0.134032, 0.126854, 0.107545, 0.08167, 0.055555, 0.033851, 0.018476, 0.009033};

void main() {
    vec4 acc = vec4(0);

    for (int s = -7; s <= 7; s += 1) {
        acc += KERNEL[s + 7] * texture(uTexture, inUV + pc.direction * float(s) / 7 / pc.resolution);
    }

    outColor = acc;
}