#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;

layout(location = 1) in vec4 inTransform01;
layout(location = 2) in vec4 inTransform2ShOf;
layout(location = 3) in vec4 inColor;
layout(location = 4) in vec4 inShadowColor;
layout(location = 5) in vec4 inBorderColor;
layout(location = 6) in vec4 inBorderRadii;
layout(location = 7) in vec4 inSizeRadThick;

layout(location = 0) out vec2 outPos;
layout(location = 1) out vec2 outSize;
layout(location = 2) out vec4 outColor;
layout(location = 3) out vec4 outShadowColor;
layout(location = 4) out vec4 outBorderColor;
layout(location = 5) out vec4 outBorderRadii;
layout(location = 6) out vec2 outShadowOffset;
layout(location = 7) out float outBlurRadius;
layout(location = 8) out float outBorderThickness;

void main() {
    outPos = inPos;
    outSize = inSizeRadThick.xy;
    outColor = inColor;
    outShadowColor = inShadowColor;
    outBorderColor = inBorderColor;
    outBorderRadii = inBorderRadii;
    outShadowOffset = inTransform2ShOf.zw;
    outBlurRadius = inSizeRadThick.z;
    outBorderThickness = inSizeRadThick.w;

    mat3 transform = mat3(mat3x2(inTransform01.xy, inTransform01.zw, inTransform2ShOf.xy));
    gl_Position = vec4(transform * vec3(inPos, 1.0), 1.0);
}